package org.libble.sample;

import org.libble.linux.BluetoothServer;
import org.libble.linux.interfaces.ServerListener;


/**
 * Example application, showcasing how the library can be used for implementing a sensor application on the raspberry pi.
 * 
 * This works together with the sensor client in the sample android application.
 */
public class SensorServerExample {
	private BluetoothServer server;
	private int clicks = 0;
	
	public SensorServerExample() {
        System.out.println("Starting server..");
		server = new BluetoothServer();
		
		server.addCallback(new ServerListener() {
			public void onMessageReceived(byte[] msg, String sender) {
				handleMessage(msg, sender);
			}

			public void onClientConnect() {
				System.out.println("Client connected");
			}

			public void onClientDisconnect() {
				System.out.println("Client disconnected");	
			}
		});
	}
	
	public void start() {
		server.start();
        System.out.println("Running..");
	}
	
	private void handleMessage(byte[] msg, String sender) {
		System.out.println("Handling request");
		String[] specifics = {"one", "two", "three", "four", "five"};
		
		byte[] val = "".getBytes();
		
		// The first byte of the message defines what kind of request is sent, and thus what to respond with
		switch(msg[0]) {
		case 0: // Latest
			clicks++;
			val = ("#clicks: " +Integer.toString(clicks)).getBytes();
			break;
		case 1: // Period
			val = "Period, huh?".getBytes();
			break;
		case 2: // specific - msg[1] is the number requested
			System.out.println("specific: " + msg[1]);
			if (msg[1] > specifics.length) {
				val = "Value too large".getBytes();
			} else {
				val = specifics[msg[1]-1].getBytes();
			}
			break;
		}
		
		server.sendMessage(val, sender);
	}
	
	public static void main(String[] args) {
		new SensorServerExample().start();
	}
}
