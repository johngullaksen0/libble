package org.libble.linux.bluetooth;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.freedesktop.dbus.interfaces.Introspectable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


// TODO: Split utils into different classes based on what they're used for, e.g. DBusUtil etc.
public class Util {
	private Util() {
	
	}
	
	/**
	 * Find nodes that descend from a DBus object, i.e. objects on its path
	 * For example, for /org/bluez/hci0, return devices that follow e.g. /org/bluez/hci0/dev_XX_XX_XX_XX_XX_XX
	 * 
	 * @param objPath base object to search from

	 * @return Found nodes
	 */
	public static ArrayList<String> getNodes(String objPath, DBusHandler dbus) {
		ArrayList<String> nodes = new ArrayList<String>();
		
		Introspectable intr = dbus.getRemoteObject(Introspectable.class, objPath);
		String xmlData = intr.Introspect();
				
		Document doc = Util.parseXml(xmlData);
		NodeList nList = doc.getElementsByTagName("node");
		
		for(int i = 0; i < nList.getLength(); i++) {
			if(nList.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element elem = (Element) nList.item(i);
				
				if (elem.hasAttribute("name")) {
					nodes.add(elem.getAttribute("name"));
				}
			}			
		}
		
		return nodes;
	}
	
	public static Document parseXml(String xmlString) {
		//Parse XML
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		dbFactory.setValidating(false);
		dbFactory.setNamespaceAware(false);
		try {
			dbFactory.setFeature("http://xml.org/sax/features/namespaces", false);
			dbFactory.setFeature("http://xml.org/sax/features/validation", false);
			dbFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
			dbFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);

			DocumentBuilder builder = dbFactory.newDocumentBuilder();
			StringBuilder sb = new StringBuilder();
			sb.append(xmlString);
			
			ByteArrayInputStream inp = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
			
			return builder.parse(inp);
			
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
