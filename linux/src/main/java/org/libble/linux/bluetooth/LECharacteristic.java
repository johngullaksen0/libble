package org.libble.linux.bluetooth;

import java.io.FileDescriptor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.bluez.GattCharacteristic1;
import org.bluez.datatypes.TwoTuple;
import org.bluez.exceptions.BluezFailedException;
import org.bluez.exceptions.BluezInProgressException;
import org.bluez.exceptions.BluezInvalidOffsetException;
import org.bluez.exceptions.BluezInvalidValueLengthException;
import org.bluez.exceptions.BluezNotAuthorizedException;
import org.bluez.exceptions.BluezNotPermittedException;
import org.bluez.exceptions.BluezNotSupportedException;
import org.freedesktop.dbus.DBusPath;
import org.freedesktop.dbus.exceptions.DBusException;
import org.freedesktop.dbus.types.UInt16;
import org.freedesktop.dbus.types.Variant;
import org.libble.linux.interfaces.CharacteristicListener;

/*
 * Local BLE characteristic 
 */
public class LECharacteristic implements GattCharacteristic1 {
	public static final String DBUS_INTERFACE_NAME = "org.bluez.GattCharacteristic1";
	
	private Map<String, Variant<?>> properties;
	private LEService parentService;
	private final String objectPath;
	private boolean notifying;
	private byte[] value; // The data represented by the characteristic
	private String uuid;

	private ArrayList<LEDescriptor> descriptors;
	
	private CharacteristicListener listener; 
	
	private static int descNum = 0; // number of descriptors, used to create dbus path
	
	/**
	 * @param path Absolute DBus-path 
	 * @param uuid Unique UUID identifying the characteristic
	 */
	public LECharacteristic(LEService parent, String path, String uuid, String[] flags) {
		objectPath = path;
		this.parentService = parent;
		this.value = "Hello from RPI".getBytes();
		this.uuid = uuid;
		this.notifying = true;
		
		properties = new HashMap<>();
		properties.put("UUID", new Variant<String>(uuid));
		properties.put("Service", new Variant<DBusPath>(new DBusPath(parentService.getObjectPath())));
		properties.put("Value", new Variant<byte[]>(value));
		properties.put("Flags", new Variant<String[]>(flags));
		
		if (Arrays.asList(flags).contains("notify")) {
			properties.put("Notifying", new Variant<Boolean>(true));
		}

		descriptors = new ArrayList<>();
	}
	
	public Map<String, Map<String, Variant<?>>> getProperties() {		
		Map<String, Map<String, Variant<?>>> outMap = new HashMap<>();
		outMap.put(DBUS_INTERFACE_NAME, properties);
		
		return outMap;
	}
	
	public void sendNotify(DBusHandler db) {
		try {
			Map<String, Variant<?>> val = new HashMap<>();	
			val.put("Value", new Variant<byte[]>(listener.onRead()));
		
			PropertiesChanged signal = new PropertiesChanged(objectPath, DBUS_INTERFACE_NAME, val, new ArrayList<String>());
			
			db.sendNotify(signal);
		} catch (DBusException e) { e.printStackTrace(); }
	}
	
	public void setListener(CharacteristicListener msgListener) {
		listener = msgListener;
	}
	
	private void setValue(byte[] val) {
		this.value = val;
		
		properties.put("Value", new Variant<byte[]>(value));
	}
	
	public boolean isRemote() {
		return false;
	}

	public String getObjectPath() {
		return objectPath;
	}
	
	public void createDescriptor(String uuid, String[] flags) {
		String path = objectPath + "/desc" + Integer.toString(descNum);
		LEDescriptor newDesc = new LEDescriptor(this, path, uuid, flags);
		
		descNum++;
		descriptors.add(newDesc);
	}
	
	public ArrayList<LEDescriptor> getDescriptors() {
		return descriptors;
	}
	
	public void export(DBusHandler dbus) {
		dbus.exportObject(this);
		
		for (LEDescriptor d : descriptors) {
			d.export(dbus);
		}
	}
	
	public String getUUID() {
		return uuid;
	}

	public Map<String, Variant<?>> GetAll(String interface_name) {
//		return getProperties().get(DBUS_INTERFACE_NAME);
		return properties;
	}

	public boolean isNotifying() {
		return notifying;
	}
	
	public void StartNotify() throws BluezFailedException, BluezNotPermittedException, 
			BluezInProgressException, BluezNotSupportedException {
		System.out.println("characteristic->StartNotify() in " + uuid);
		
		if (!notifying) {
			notifying = true;
		}
	}
	
	public void StopNotify() throws BluezFailedException { 
		System.out.println("Characteristic->StopNotify(): " + uuid);
		
		if (notifying) {
			notifying = false;
		}
	}
	
	// Called when requesting to read the value
	public byte[] ReadValue(Map<String, Variant<?>> _options)
			throws BluezFailedException, BluezInProgressException, BluezNotPermittedException,
			BluezNotAuthorizedException, BluezInvalidOffsetException, BluezNotSupportedException {
		System.out.println("Characteristic->ReadValue(): " + uuid);
		
		if (listener != null) {
			value = listener.onRead();
			setValue(value);
			
			//sendNotify();
			
			return value;
		} else {
			throw new RuntimeException("Read from char '" + uuid +  "' without listener!");
		}
	}

	// Called when requesting to write to the value
	public void WriteValue(byte[] value, Map<String, Variant<?>> options)
			throws BluezFailedException, BluezInProgressException, BluezNotPermittedException,
			BluezInvalidValueLengthException, BluezNotAuthorizedException, BluezNotSupportedException {
		System.out.println("Characteristic->WriteValue(): " + uuid);
		
		if (listener != null) {
			listener.onWrite(value);
		} else {
			throw new RuntimeException("Write from char '" + uuid +  "' without listener!");
		}
	}

	// TODO: might need to implement some of these?
	// If not, don't make them part of the interface when we add our own interface implementations
	public TwoTuple<FileDescriptor, UInt16> AcquireWrite(Map<String, Variant<?>> _options)
			throws BluezFailedException, BluezNotSupportedException {System.out.println("AcquireWrite called!");return null;}
	
	public TwoTuple<FileDescriptor, UInt16> AcquireNotify(Map<String, Variant<?>> _options)
			throws BluezFailedException, BluezNotSupportedException {System.out.println("AcquireNotify called!");return null;}
	public <A> A Get(String interface_name, String property_name) { System.out.println("Characteristic->Get() called!");return null;}
	public <A> void Set(String interface_name, String property_name, A value) {System.out.println("Characteristic->Set() called!");}
	
	// Server only -  message from client confirming that a value was received
	public void Confirm() throws BluezFailedException {System.out.println("Characteristic->confirm received!");}
}