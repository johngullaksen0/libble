package org.libble.linux.bluetooth;

import java.io.IOException;
import org.freedesktop.dbus.connections.impl.DBusConnection;
import org.freedesktop.dbus.exceptions.DBusException;
import org.freedesktop.dbus.interfaces.DBusInterface;
import org.freedesktop.dbus.interfaces.Properties;
import org.freedesktop.dbus.messages.Message;

/**
 * Handler for all direct communication with the DBus
 */
public class DBusHandler {
	public static final String BLUEZ_DBUS_NAME = "org.bluez";
	public static final String BASE_OBJECT_PATH = "/org/bluez/hci0"; // Not guaranteed to be standard on all RPI setups, might have to detect this

	private DBusConnection connection;

	public DBusHandler() {
		try {
			connection = DBusConnection.getConnection(DBusConnection.DEFAULT_SYSTEM_BUS_ADDRESS);
			
			if(!connection.isConnected()) {
				System.out.println("Could not connect to DBUs bus");
			}
		} catch (DBusException e) {
			e.printStackTrace();
		}
	}
	
	public void sendNotify(Message msg) {
		connection.sendMessage(msg);
	}
	
	/**
	 *  This method is temporary and won't be necessary in the future - all DBus functions should be implemented as methods in this class
	 */
	public DBusConnection getConnection() {
		return connection;
	}
	
	public void exportObject(DBusInterface obj) {
		try {
			connection.exportObject(obj.getObjectPath(), obj);
		} catch (DBusException e) {
			e.printStackTrace();
		}
	}
	
	public void unexportObject(DBusInterface obj) {
		connection.unExportObject(obj.getObjectPath());
	}

	/**
	 * Get a remote DBus object of the specified type from the base object path, ie. "/org/bluez/hci0"
	 * 
	 * @param type Class type
	 * @return The remote object
	 */
	public <T extends DBusInterface> T getRemoteObject(Class<T> type) {
		return getRemoteObject(type, BASE_OBJECT_PATH);
	}
	
	public <T extends DBusInterface> T getRemoteObject(Class<T> type, String objPath) {
		try {
			return connection.getRemoteObject(BLUEZ_DBUS_NAME, objPath, type);
		} catch (DBusException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public <T> T getProperty(String interfaceName, String field, Class<T> type) {
		return getProperty(BASE_OBJECT_PATH, interfaceName, field, type);
	}
	
	
	public <T> T getProperty(String objPath, String interfaceName, String field, Class<T> type) {
		try {
			Properties props = (Properties) connection.getRemoteObject(BLUEZ_DBUS_NAME, objPath, Properties.class);
			Object obj = props.Get(interfaceName, field);
			
			return type.cast(obj);
		} catch (DBusException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void setProperty(String interfaceName, String field, Object value) {
    	try {
			Properties props = (Properties) connection.getRemoteObject(BLUEZ_DBUS_NAME, BASE_OBJECT_PATH, Properties.class);
			
	    	props.Set(interfaceName, field, value);
			
		} catch (DBusException e) {
			e.printStackTrace();
		}
	}
	
	public void close() {
		try {
			connection.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}