package org.libble.linux.bluetooth;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.bluez.Adapter1;
import org.bluez.Device1;
import org.bluez.GattManager1;
import org.bluez.LEAdvertisingManager1;
import org.bluez.exceptions.BluezAlreadyExistsException;
import org.bluez.exceptions.BluezDoesNotExistException;
import org.bluez.exceptions.BluezFailedException;
import org.bluez.exceptions.BluezInvalidArgumentsException;
import org.bluez.exceptions.BluezInvalidLengthException;
import org.bluez.exceptions.BluezNotAuthorizedException;
import org.bluez.exceptions.BluezNotPermittedException;
import org.bluez.exceptions.BluezNotReadyException;
import org.bluez.exceptions.BluezNotSupportedException;
import org.freedesktop.dbus.DBusPath;
import org.freedesktop.dbus.exceptions.DBusException;
import org.freedesktop.dbus.interfaces.DBusSigHandler;
import org.freedesktop.dbus.interfaces.ObjectManager;
import org.freedesktop.dbus.types.UInt32;
import org.freedesktop.dbus.types.Variant;
import org.libble.linux.Constants;
import org.libble.linux.interfaces.CharacteristicListener;
import org.libble.linux.interfaces.DeviceConnectionListener;

/*
 *  Class representing our connection to bluez - basically a wrapper around the blueZ DBus API
 */

public class BluetoothConnection implements ObjectManager {		
	private Adapter1 rawAdapter;
	private DBusHandler dbus;
	
	private LEAdvertisement advertisement;
	private ArrayList<LEService> services;
	
	private String address;
	
	public BluetoothConnection(DeviceConnectionListener devCallback) {
		dbus = new DBusHandler();		
		services = new ArrayList<>();
	    rawAdapter = dbus.getRemoteObject(Adapter1.class);
	    
	    // Power on and set discoverable 
	    dbus.setProperty("org.bluez.Adapter1", "Powered", true);
	    dbus.setProperty("org.bluez.Adapter1", "Discoverable", true);
	    
	    address = dbus.getProperty("org.bluez.Adapter1", "Address", String.class);
	    
	    UInt32 timeout = new UInt32("0");
	    dbus.setProperty("org.bluez.Adapter1", "DiscoverableTimeout", timeout);
	    
		setDiscoveryFilter(Constants.SERVICE_UUID);
	    
	    clearDevices();
		
	    // Setup signal handlers
	    initSignalHandlers(devCallback);
	}
	
	private void initSignalHandlers(DeviceConnectionListener devCallback) {
		ObjectManager bluezObjectManager = dbus.getRemoteObject(ObjectManager.class, "/");
		
	    DBusSigHandler<InterfacesAdded> addedHandler = new DBusSigHandler<InterfacesAdded>() {
			public void handle(InterfacesAdded s) {
				Map<String, Variant<?>> device = s.getInterfaces().get("org.bluez.Device1"); // We only care about added devices
				
				if(device != null) {				
					Device1 dev = dbus.getRemoteObject(Device1.class, s.getSignalSource().toString());
					
					devCallback.onDeviceDiscovered(dev);
				} 
			}
	    };
	    
	    DBusSigHandler<InterfacesRemoved> removedHandler = new DBusSigHandler<InterfacesRemoved>() {
			public void handle(InterfacesRemoved s) {				
				for (String i : s.getInterfaces()) {
					if (i.equals("org.bluez.Device1")) {
						devCallback.onDeviceRemoved(null);
					}
				}
			}
	    };
	      
	    try {
			dbus.getConnection().addSigHandler(InterfacesAdded.class, bluezObjectManager, addedHandler);
			dbus.getConnection().addSigHandler(InterfacesRemoved.class, bluezObjectManager, removedHandler);
		} catch (DBusException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * Removes all previously discovered devices - this is necessary to be notified when devices are discovered
	 */
	private void clearDevices() {
		System.out.println("Clearing previous devices");
		
		ArrayList<String> devicePaths = Util.getNodes(DBusHandler.BASE_OBJECT_PATH, dbus);
		for(String d : devicePaths) {
			String path = DBusHandler.BASE_OBJECT_PATH+ "/" + d;
			
			removeDevice(path);
		}
	}
	
	/**
	 * Notify that a characteristic has changed
	 */
	public void sendNotify() {
		for (LECharacteristic c :services.get(0).getCharacteristics()) {
			if (Constants.CHARACTERISTIC_TIME_UUID.equals(c.getUUID()) && c.isNotifying()) {
				c.sendNotify(dbus);
			}
		}
	}
	
	/**
	 * Remove a specific device 
	 * 
	 * @param path DBus path to device
	 */
	public void removeDevice(String path) {
		try {
			rawAdapter.RemoveDevice(new DBusPath(path));
		} catch (BluezInvalidArgumentsException | BluezFailedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Create local GATT service and register it with blueZ
	 */
	public void setupGATTService(CharacteristicListener cl) {
		GattManager1 gattManager = dbus.getRemoteObject(GattManager1.class);
		
		LEService service = new LEService(Constants.DBUS_OBJECT_BASE, Constants.SERVICE_UUID);
		
		String[] msgFlags = {"read", "write", "notify"};
		service.createCharacteristic(Constants.CHARACTERISTIC_MSG_UUID, msgFlags, cl);
		
		String[] timeFlags = {"read", "write", "notify"};
		service.createCharacteristic(Constants.CHARACTERISTIC_TIME_UUID, timeFlags, cl);
		
		// Add descriptor to TIME characteristic
		String[] timeDescFlags = {"read", "write"};
		
		for (LECharacteristic c : service.getCharacteristics()) {
			if (c.getUUID() == Constants.CHARACTERISTIC_TIME_UUID) {
				c.createDescriptor(Constants.TIME_DESCRIPTOR_UUID, timeDescFlags);
			}
		}

		services.add(service);
		
		service.export(dbus);
		dbus.exportObject(this);
		
		try {
			gattManager.RegisterApplication(new DBusPath(getObjectPath()), new HashMap<String, Variant<?>>());
		} catch (BluezInvalidArgumentsException | BluezAlreadyExistsException e) {
			e.printStackTrace();
		}
	}
	
	public void startAdvertising() {
		if(services.isEmpty()) {
			throw new RuntimeException("Services must be initialized before advertising!");
		}
			
		LEAdvertisingManager1 advManager = dbus.getRemoteObject(LEAdvertisingManager1.class);
		advertisement = new LEAdvertisement(Constants.DBUS_OBJECT_BASE + "/advertisement", services);
		dbus.exportObject(advertisement);
		
		// Register the advertisement with the bluez daemon
		try {
			advManager.RegisterAdvertisement(new DBusPath(advertisement.getObjectPath()), new HashMap<String, Variant<?>>());
		} catch (BluezInvalidArgumentsException | BluezAlreadyExistsException | BluezInvalidLengthException
				| BluezNotPermittedException e) {
			e.printStackTrace();
		}
	}
	
	public void stopAdvertising() {
		LEAdvertisingManager1 advManager = dbus.getRemoteObject(LEAdvertisingManager1.class);
		
		if (advertisement != null) {
			try {
				advManager.UnregisterAdvertisement(new DBusPath(advertisement.getObjectPath()));
			} catch (BluezInvalidArgumentsException | BluezDoesNotExistException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void startDiscovery() {		
		try {
			rawAdapter.StartDiscovery();
		} catch (BluezNotReadyException | BluezFailedException e) {
			e.printStackTrace();
		}
	}
	
	public void stopDiscovery() {
		try {
			rawAdapter.StopDiscovery();
		} catch (BluezNotReadyException | BluezFailedException | BluezNotAuthorizedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Enable filtering discovery on a specific service UUID
	 * 
	 * @param serviceUUIDs UUID to filter on
	 */
	private void setDiscoveryFilter(String serviceUUIDs) {
		Map<String, Variant<?>> filter = new HashMap<>();
		
		String[] uuids = {serviceUUIDs};
		
		filter.put("UUIDs", new Variant<String[]>(uuids));

		try {
			rawAdapter.SetDiscoveryFilter(filter);
		} catch (BluezNotReadyException | BluezNotSupportedException | BluezFailedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Reset the discovery filter to default, ie. discover all available devices
	 */
	private void clearDiscoveryFilter() {
		try {
			rawAdapter.SetDiscoveryFilter(new HashMap<String, Variant<?>>());
		} catch (BluezNotReadyException | BluezNotSupportedException | BluezFailedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Get all discovered devices 
	 *
	 * @return Arraylist of remote devices
	 */
	public ArrayList<LEDevice> getDiscoveredDevices() {
		ArrayList<LEDevice> devices = new ArrayList<>();

		ArrayList<String> devicePaths = Util.getNodes(DBusHandler.BASE_OBJECT_PATH, dbus);

		for(String d : devicePaths) {
			Device1 dev = dbus.getRemoteObject(Device1.class, DBusHandler.BASE_OBJECT_PATH+ "/" + d);
			devices.add(new LEDevice(dev, dbus));
		}
		
		return devices;
	}
	
	/**
	 * Get local address
	 * 
	 * @return address
	 */
	public String getAddress() {
		return address;
	}
	
	/**
	 * Close the connection to the DBus and everything related to it 
	 */
	public void close() {
		clearDiscoveryFilter();
		clearDevices();
		
		stopAdvertising();
		
		dbus.close();
	}

	// Methods required by the ObjectManager interface
	public Map<DBusPath, Map<String, Map<String, Variant<?>>>> GetManagedObjects() {
		Map<DBusPath, Map<String, Map<String, Variant<?>>>> managedObjects = new HashMap<>();
		
		for (LEService s : services) {
			managedObjects.put(new DBusPath(s.getObjectPath()), s.getProperties());
			for(LECharacteristic c : s.getCharacteristics()) {
				managedObjects.put(new DBusPath(c.getObjectPath()), c.getProperties());
				for (LEDescriptor d : c.getDescriptors()) {
					managedObjects.put(new DBusPath(d.getObjectPath()), d.getProperties());
				}
			}			
		}
		
		return managedObjects;
	}
	
	public boolean isRemote() {
		return false;
	}

	public String getObjectPath() {
		return Constants.DBUS_OBJECT_BASE;
	}
}