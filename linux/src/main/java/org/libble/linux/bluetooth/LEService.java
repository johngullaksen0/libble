package org.libble.linux.bluetooth;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.bluez.GattService1;
import org.freedesktop.dbus.types.Variant;
import org.libble.linux.interfaces.CharacteristicListener;

/**
 * Local BLE Service
 */
public class LEService implements GattService1 {
	public static final String DBUS_INTERFACE_NAME = "org.bluez.GattService1";
	
	private final boolean IS_PRIMARY = true; // This will need to be changed if we add more services
	private final String objectPath;
	private final String uuid;
	
	// Used to set the service and characteristic path - incremented once for each service and char
	private static int serviceNum = 0;
	private int charNum = 0;
	
	private ArrayList<LECharacteristic> characteristics;
	private Map<String, Variant<?>> properties;
		
	/**
	 * @param basePath	DBus base object path, which we construct our path from
	 * @param uuid	Unique ID, identifying the service. Format: xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
	 */
	public LEService(String basePath, String uuid) {
		objectPath = basePath + "/service" + Integer.toString(serviceNum);
		serviceNum++;
		
		this.uuid = uuid;
		
		properties = new HashMap<>();
		properties.put("Primary", new Variant<Boolean>(IS_PRIMARY));
		properties.put("UUID", new Variant<String>(uuid));
		properties.put("Characteristics", new Variant<String>(""));
		
		characteristics = new ArrayList<>();
	}
	
	public void createCharacteristic(String uuid, String[] flags, CharacteristicListener ml) {
		String path = objectPath + "/char" + Integer.toString(charNum);
		LECharacteristic newChar = new LECharacteristic(this, path, uuid, flags);
		newChar.setListener(ml);
		
		charNum++;
		characteristics.add(newChar);
	}
	
	public ArrayList<LECharacteristic> getCharacteristics() {
		return characteristics;
	}

	public Map<String, Map<String, Variant<?>>> getProperties() {		
		Map<String, Map<String, Variant<?>>> outMap = new HashMap<>();
		outMap.put(DBUS_INTERFACE_NAME, properties);
		return outMap;
	}
	
	/**
	 * Export the Service and its characteristics to the DBus
	 * 
	 * @param dbus DBus connection to export to
	 */
	public void export(DBusHandler dbus) {
		dbus.exportObject(this);
		
		for(LECharacteristic le : characteristics) {
			le.export(dbus);
		}
	}
	
	public String getUUID() {
		return uuid;
	}
	
	public boolean isRemote() {
		return false;
	}

	public String getObjectPath() {
		return objectPath;
	}

	public Map<String, Variant<?>> GetAll(String interface_name) {
		return properties;
	}
	
	// Required by the interface
	public <A> A Get(String interface_name, String property_name) { 
		return null;
	}
	public <A> void Set(String interface_name, String property_name, A value) {
	}
}