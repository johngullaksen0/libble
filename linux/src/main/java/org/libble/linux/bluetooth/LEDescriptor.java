package org.libble.linux.bluetooth;

import java.util.HashMap;
import java.util.Map;

import org.bluez.GattDescriptor1;
import org.bluez.exceptions.BluezFailedException;
import org.bluez.exceptions.BluezInProgressException;
import org.bluez.exceptions.BluezInvalidValueLengthException;
import org.bluez.exceptions.BluezNotAuthorizedException;
import org.bluez.exceptions.BluezNotPermittedException;
import org.bluez.exceptions.BluezNotSupportedException;
import org.freedesktop.dbus.DBusPath;
import org.freedesktop.dbus.types.Variant;

public class LEDescriptor implements GattDescriptor1 {
	private static final String DBUS_INTERFACE_NAME = "org.bluez.GattDescriptor1";
	
	private String objectPath;
	private String uuid;
	
	private byte[] data;
	
	private Map<String, Variant<?>> properties;
	
	public LEDescriptor(LECharacteristic parent, String path, String uuid,  String[] flags) {
		this.objectPath = path;
		this.uuid = uuid;
		
		data = "2902".getBytes(); // TODO: change this, might actually be needed for something
		
		properties = new HashMap<>();
		
		properties.put("UUID", new Variant<String>(uuid));
		properties.put("Characteristic", new Variant<DBusPath>(new DBusPath(parent.getObjectPath())));
		properties.put("Value", new Variant<byte[]>(data));
		properties.put("Flags", new Variant<String[]>(flags));
	}
	
	public Map<String, Map<String, Variant<?>>> getProperties() {		
		Map<String, Map<String, Variant<?>>> outMap = new HashMap<>();
		outMap.put(DBUS_INTERFACE_NAME, properties);
		
		return outMap;
	}
	
	public boolean isRemote() {
		return false;
	} 
	
	public String getUUID() {
		return uuid;
	}

	public String getObjectPath() {
		return objectPath;
	}
	
	public void export(DBusHandler dbus) {
		dbus.exportObject(this);
	}

	public Map<String, Variant<?>> GetAll(String interface_name) {
		return properties;
	}
	
	/*
	 * Required by interface; not in use
	 */
	public byte[] ReadValue(Map<String, Variant<?>> _flags) throws BluezFailedException, BluezInProgressException,
			BluezNotPermittedException, BluezNotAuthorizedException, BluezNotSupportedException {
		return null;
	}
	public void WriteValue(byte[] _value, Map<String, Variant<?>> _flags)
			throws BluezFailedException, BluezInProgressException, BluezNotPermittedException,
			BluezInvalidValueLengthException, BluezNotAuthorizedException, BluezNotSupportedException {
	}
	public <A> A Get(String interface_name, String property_name) {
		return null;
	}
	public <A> void Set(String interface_name, String property_name, A value) {}
}
