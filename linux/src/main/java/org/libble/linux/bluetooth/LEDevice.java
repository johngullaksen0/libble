package org.libble.linux.bluetooth;

import java.util.ArrayList;
import java.util.HashMap;

import org.bluez.Device1;
import org.bluez.GattCharacteristic1;
import org.bluez.GattService1;
import org.bluez.exceptions.BluezAlreadyConnectedException;
import org.bluez.exceptions.BluezFailedException;
import org.bluez.exceptions.BluezInProgressException;
import org.bluez.exceptions.BluezInvalidValueLengthException;
import org.bluez.exceptions.BluezNotAuthorizedException;
import org.bluez.exceptions.BluezNotConnectedException;
import org.bluez.exceptions.BluezNotPermittedException;
import org.bluez.exceptions.BluezNotReadyException;
import org.bluez.exceptions.BluezNotSupportedException;
import org.freedesktop.dbus.errors.NoReply;
import org.freedesktop.dbus.exceptions.DBusExecutionException;
import org.freedesktop.dbus.types.Variant;
import org.libble.linux.Constants;

/*
 * 	Remote BLE device
 * 
 * 	TODO: Rename class to represent it being remote eg. RemoteLEDevice
 */
public class LEDevice {
	public static final String DBUS_INTERFACE_NAME = "org.bluez.Device1";
	private final String objectPath;
	
	private Device1 rawDevice;
	private DBusHandler dbus;
	
	private GattCharacteristic1 msgCharacteristic;

	private String address;
	
	public LEDevice(Device1 dev, DBusHandler db) {
		rawDevice = dev;
		dbus = db;
		
		objectPath = dev.getObjectPath();
		address = dbus.getProperty(objectPath, DBUS_INTERFACE_NAME, "Address", String.class);
	}
	
	/**
	 * 
	 * @return true if characteristics were found, false if not
	 */
	public boolean initCharacteristics() {
		GattService1 gs = getServiceByUUID(Constants.SERVICE_UUID);
		
		if (gs == null) {
			return false;
		}
		
		ArrayList<String> deviceNodes = Util.getNodes(gs.getObjectPath(), dbus); // Get DBus names of the characteristics of our service
		
		for (String s : deviceNodes) { 
			String charObjectPath = gs.getObjectPath() + "/" + s;

			String charUUID = dbus.getProperty(charObjectPath, LECharacteristic.DBUS_INTERFACE_NAME, "UUID", String.class); 
			
			if (charUUID.equals(Constants.CHARACTERISTIC_MSG_UUID)) {
				msgCharacteristic = dbus.getRemoteObject(GattCharacteristic1.class, charObjectPath);

				return true;
			}
		}
		return false;
	}
	
	public void sendMessage(String msg) {
		if (msgCharacteristic == null) {
			throw new IllegalStateException("Trying to send message to non-initialized device!");
		}
		
		try {
//			byte[] val = echoChar.ReadValue(new HashMap<String, Variant<?>>());
			
			msgCharacteristic.WriteValue(msg.getBytes(), new HashMap<String, Variant<?>>());
			
		} catch (BluezFailedException | BluezInProgressException | BluezNotPermittedException
				| BluezNotAuthorizedException | BluezNotSupportedException | BluezInvalidValueLengthException e) {
			e.printStackTrace();
		}

	}
	
	public String getAddress() {
		return address;
	}
	
	/**
	 * Get a service provided by the device, by its UUID
	 * 
	 * @param uuid Service UUID
	 * @return The service if it exists, or null if it doesn't
	 */
	public GattService1 getServiceByUUID(String uuid) {
		
		// Get all services belonging to this device
		for (String s : Util.getNodes(objectPath, dbus)) {
			String serviceObjectPath = objectPath+ "/" + s;
						
			String serviceUUID = dbus.getProperty(serviceObjectPath, LEService.DBUS_INTERFACE_NAME, "UUID", String.class);

			if (serviceUUID.equals(uuid)) {
				return dbus.getRemoteObject(GattService1.class, serviceObjectPath);
			}			
		}
		return null;
	}
	
	public boolean isConnected() {
		return dbus.getProperty(objectPath, DBUS_INTERFACE_NAME, "Connected", Boolean.class);
	}
	
	public boolean connect()  {
		try{
			rawDevice.Connect();
		} catch (BluezNotReadyException | BluezFailedException | BluezInProgressException e) {
			e.printStackTrace();
	
		} catch (BluezAlreadyConnectedException e) {
			return true;
		
		/*
		 * NoReply is often thrown when two nodes attempt to connect to each other simultaneously 
		 * The default timeout is 20s, and reducing this would be nice, though that will require digging into the DBus-java source
		 * TODO: look into this
		 */
		} catch(NoReply n) { 
			System.out.println("Error connecting to " + objectPath + ": No Reply");
			return isConnected();
		} catch (DBusExecutionException e) {
			System.out.println("DBusExecutionException: " + e.getMessage());
			return isConnected();
		}
		
		return true;
	}
	
	public void disconnect() {
		try {
			rawDevice.Disconnect();
		} catch (BluezNotConnectedException e) {
			System.out.println("Not connected, ignoring attempt to disconnect");
		}		
	}
	
	public String getObjectPath() {
		return objectPath;
	}
}
