package org.libble.linux.bluetooth;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.bluez.LEAdvertisement1;
import org.freedesktop.dbus.interfaces.Properties;
import org.freedesktop.dbus.types.Variant;
import org.libble.linux.Constants;

public class LEAdvertisement implements LEAdvertisement1, Properties{
	public static final String GATT_INTERFACE = "org.bluez.LEAdvertisement1";
	
	private final String ADVERTISEMENT_TYPE = "broadcast"; // This can either be "broadcast" or "peripheral" - afaik, we will always use "broadcast"
	private final String objectPath;
	private Map<String, Variant<?>> properties;

	public LEAdvertisement(String objectPath, ArrayList<LEService> services) {
		this.objectPath = objectPath;
		
		// Setup the properties that define the advertisement
		properties = new HashMap<String, Variant<?>>();
		
		ArrayList<String> serviceUUIDs = new ArrayList<>();
		for(LEService l : services) {
			serviceUUIDs.add(l.getUUID());
		}
		
		String[] uuids = serviceUUIDs.toArray(new String[0]);
		properties.put("ServiceUUIDs", new Variant<String[]>(uuids));
		properties.put("Type", new Variant<String>(ADVERTISEMENT_TYPE));
		properties.put("LocalName", new Variant<String>(Constants.DEVICE_NAME));
	}
	
	public String getObjectPath() {
		return objectPath;
	}
	
	// Get all properties - this is in turn called by BlueZ to define how the advertisement takes place
	public Map<String, Variant<?>> GetAll(String interface_name) {		
		return properties;
	}
	
	// Get a single property
	public <A> A Get(String interface_name, String property_name) {
		return null;
	}

	// Set a single property
	public <A> void Set(String interface_name, String property_name, A value) {
	}

	public boolean isRemote() {
		return false;
	}
	
	// This is called when the advertisement is removed, ie. when UnregisterAdvertisement is called on LEManager. 
	// Cleanup should be done here if need be
	public void Release() {
		
	}
}
