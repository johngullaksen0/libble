package org.libble.linux;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.bluez.Device1;
import org.libble.linux.bluetooth.BluetoothConnection;
import org.libble.linux.bluetooth.LEDevice;
import org.libble.linux.interfaces.Client;
import org.libble.linux.interfaces.ClientListener;
import org.libble.linux.interfaces.DeviceConnectionListener;

/**
 * Linux implementation of the Client interface
 * 
 * Some of these methods are not implemented, largely due to differences in the BLE API on android and bluez
 */
public class BluetoothClient implements Client {
	private BluetoothConnection btConnection;
	
	private ArrayList<ClientListener> listeners;
	private DeviceConnectionListener deviceListener;
	private LEDevice server = null;

	private boolean connected = false;
	private boolean scanning = false;		
	
	public BluetoothClient() {		
		initListeners();
		
		listeners = new ArrayList<>();

		btConnection = new BluetoothConnection(deviceListener);		
	}
	
	private void initListeners() {
		deviceListener = new DeviceConnectionListener() {
			public void onDeviceDiscovered(Device1 dev) {
				for (ClientListener cl : listeners) {
					List<String> devs = btConnection.getDiscoveredDevices().stream().map(x -> x.getAddress()).collect(Collectors.toList());
					
					/*
					 *  A quirk on linux; there isn't really a period of scanning, as in Android.
					 *  so onScanComplete() is called every time a new device is discovered, however,
					 *  this should not be a problem for client applications..
					 */
					cl.onScanComplete(devs);
				}
			}
			
			public void onDeviceRemoved(Device1 dev) {
				for (ClientListener cl : listeners) {
					if (dev.Get("org.bluez.Device1", "Address").equals(server.getAddress())) {
						cl.onClientDisconnect();						
						connected = false;
					}
				}
			}			
		};	
	}
	
	public void startScan() {
		btConnection.startDiscovery();
		scanning = true;
		System.out.println("Scanning..");
	}

	public void stopScan() {
		btConnection.stopDiscovery();
		scanning = false;
	}

	/*
	 * TODO: not yet implemented
	 */
	public void startAddressScan(String addr) {
		startScan();
	}

	/*
	 * TODO: not yet implemented
	 */
	public void startDeviceScan(String devName) {
		startScan();
	}

	public void connectDevice(String addr) {
		for (LEDevice d: btConnection.getDiscoveredDevices()) {
			if (d.getAddress() == addr) {
				if(d.connect()) {
					connected = true;
				}
			}
		}
		connected = false;
	}
	
	public void disconnect() {
		if (server != null) {
			server.disconnect();
			server = null;
		}
	}

	public void sendMessage(byte[] msg) {
		sendMessage(msg.toString());
	}
	
	public void sendMessage(String msg) {
		if (server != null) {
			server.sendMessage(msg);
		}
	}

	public boolean isConnected() {
		return connected && server == null;
	}

	public String getAddress() {
		return btConnection.getAddress();
	}

	public String getServerAddress() {
		if (server != null) {
			return server.getAddress();
		}
		return null;
	}

	public boolean isScanning() {
		return scanning;
	}

	public String getDeviceName() {
		return Constants.DEVICE_NAME;
	}

	/*
	 * TODO: not implemented
	 */
	public byte[] handleMessage(byte[] msg) {
		return null;
	}

	public void addCallback(ClientListener cl) {
		if (!listeners.contains(cl)) {
			listeners.add(cl);
		}
	}

	public void removeCallback(ClientListener cl) {
		if (listeners.contains(cl)) {
			listeners.remove(cl);
		}
	}
}