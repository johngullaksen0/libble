package org.libble.linux;

import java.util.ArrayList;
import java.util.Map;

import org.bluez.Device1;
import org.libble.linux.bluetooth.BluetoothConnection;
import org.libble.linux.bluetooth.LEDevice;
import org.libble.linux.interfaces.DeviceConnectionListener;

/**
 * Represents a connection to the network
 */
public class BluetoothNetwork {
//	private BluetoothConnection bt;
//	private volatile ArrayList<LEDevice> connectedNodes;
//	
//	private DeviceConnectionListener deviceListener;
//	private NetworkConnectionListener connectionListener;
//			
//	/**
//	 * Prepare everything necessary to connect to the network; setup GATT profiles, connect to bluez
//	 */
//	public BluetoothNetwork(MessageListener msgListener) {
//		System.out.println("Initializing local network node");
//		
//		connectedNodes = new ArrayList<>();
//		createDeviceCallback();
//		
//		bt = new BluetoothConnection(deviceListener);
//		bt.setupGATTService(msgListener);	
//	}
//	
//	private void createDeviceCallback() {
//		deviceListener = new DeviceConnectionListener() {
//
//			public void onDeviceDiscovered(LEDevice dev) {
//
//				if (!connectDevice(dev)) {
//					return;
//				}
//				
//				// We poll for characteristics, as it may take some time before they're available from DBus
//				while(!dev.initCharacteristics()) {
//					System.out.println("Characteristic not ready, retrying");
//					try {
//						Thread.sleep(500);
//					} catch (InterruptedException e) {}
//				}
//				
//				connectedNodes.add(dev);
//				if(connectionListener != null) {
//					connectionListener.onConnect();									
//				}
//				
//			}
//			public void onDeviceRemoved(Device1 dev) {
//				//TODO: implement - remove from connectedNodes etc.
//			}
//		};
//	}
//	
//	/**
//	 * Initialize connection to the network
//	 */
//	public void initConnection() {
//		System.out.println("Connecting to network.. ");
//		
//		bt.startDiscovery();
//		bt.startAdvertising();
//	}
//	
//	
//	private void removeDevice(LEDevice dev) {
//		bt.removeDevice(dev.getObjectPath());
//		
//		connectedNodes.remove(dev);
//	}
//	
//	/**
//	 * Connect to a specific device
//	 * @param dev LEDevice to connect to
//	 * 
//	 * @return true on successful connection
//	 */
//	public boolean connectDevice(LEDevice dev) {
//		int attempts = 0;
//		
//		System.out.println("Connecting to device..");
//		
//		while(attempts < Constants.RECONNECT_ATTEMPTS) {
//			if(dev.connect()) {
//				break;
//			} else {
//				System.out.println("Could not connect to device, retrying in " + Constants.RECONNECT_DELAY + " ms");
//				attempts++;
//				try {
//					Thread.sleep(Constants.RECONNECT_DELAY);
//				} catch (InterruptedException e) {
//				}
//			}
//		}
//		
//		if(dev.isConnected()) {
//			return true;
//			
//		} else {
//			System.out.println("Ignoring connecting to device " + " after " + Constants.RECONNECT_ATTEMPTS + "attempts");
//			return false;
//		}
//			
//	}
//	/**
//	 * Set the callback to be called upon connecting and disconnecting from the network
//	 * 
//	 * @param callback
//	 */
//	public void setConnectionCallback(NetworkConnectionListener callback) {
//		this.connectionListener = callback;
//	}
//	
//	public void disconnect() {
//		if (isConnected()) {
//			for(LEDevice dev : connectedNodes) {
//				dev.disconnect();
//			}
//			bt.close();
//		}
//		
//		if(connectionListener != null) {
//			connectionListener.onDisconnect();			
//		}
//	}
//
//	public boolean isConnected() {
//		if (connectedNodes.size() == 0) {
//			return false;
//		}
//		
//		boolean flag = false;
//		for (LEDevice dev : connectedNodes) {
//			if (dev.isConnected()) {
//				flag = true;
//			} else {
//				removeDevice(dev);
//			}
//		}
//		
//		return flag;
//	}
//	
//	// Send a message - returns true if message was successfully sent
//	// TODO: Handle exceptions when not connected, though this is basically a problem of not properly maintaining the list of connected nodes
//	public boolean sendMessage(String msg) {
//		if(!isConnected()) {
//			System.out.println("Can't send message - not connected to network");
//			return false;
//		} else {		
//			for (LEDevice dev : connectedNodes) {
//				dev.sendMessage(msg);			
//			}
//		}
//		return true;
//	}
//	
//	public ArrayList<String> getConnectedNode() {
//		return null;
//	}
//	
//	public Map<String, String> getNetworkTopology() {
//		return null;
//	}
}
