package org.libble.linux;

import java.util.ArrayList;

import org.bluez.Device1;
import org.libble.linux.bluetooth.BluetoothConnection;
import org.libble.linux.interfaces.CharacteristicListener;
import org.libble.linux.interfaces.DeviceConnectionListener;
import org.libble.linux.interfaces.Server;
import org.libble.linux.interfaces.ServerListener;

/**
 * Linux implementation of the Server interface
 */
public class BluetoothServer implements Server {
	private BluetoothConnection btConnection;
	private ArrayList<Device1> connectedDevices;
	
	private ArrayList<ServerListener> listeners;
	
	private DeviceConnectionListener deviceListener;
	private CharacteristicListener charListener;
	
	private byte[] value;
	
	public BluetoothServer() {
		listeners = new ArrayList<>();
		connectedDevices = new ArrayList<>();
		
		initServerListeners();
		
		btConnection = new BluetoothConnection(deviceListener);
		btConnection.setupGATTService(charListener);
	}
	
	/**
	 * Initialize the internally used callbacks
	 */
	private void initServerListeners() {
		deviceListener = new DeviceConnectionListener() {
			public void onDeviceDiscovered(Device1 dev) {
				deviceConnected(dev);
			}
			public void onDeviceRemoved(Device1 dev) {
				deviceRemoved(dev);
			}			
		};	
		
		charListener = new CharacteristicListener() {
			public byte[] onRead() {
				System.out.println("Characteristiclistener->Read");
				return value;
			}

			public void onWrite(byte[] value) {
				for (ServerListener sl : listeners) {
					sl.onMessageReceived(value, "");
				}
			}			
		};		
	}
	
	private void deviceConnected(Device1 dev) {		
		System.out.println("Device connected");
		if (!connectedDevices.contains(dev)) {
			connectedDevices.add(dev);			
		}
	}

	private void deviceRemoved(Device1 dev) {
		System.out.println("Device disconnected");
		if (connectedDevices.contains(dev)) {
			connectedDevices.remove(dev);			
		}
	}
	
	public void start() {
		btConnection.startAdvertising();
	}

	public void stop() {
		btConnection.stopAdvertising();
		btConnection.close();
	}

	public void restartServer() {
		stop();
		start();
	}

	/*
	 * The way we relay messages back to the client is by notifying that our characteristic is changed. 
	 * Unfortunately, due to the way notifying is implemented in bluez, it's probably not possible to only
	 * notify a single client, as they are all listening to the same characteristic. Because of this, 
	 * we override this to simply call the method which floods the message to every client.
	 */
	public void sendMessage(byte[] msg, String addr) {
		floodMessage(msg);
	}
	
	public void floodMessage(byte[] msg) {
		value = msg;
		btConnection.sendNotify();
	}

	public void addCallback(ServerListener sl) {
		if (!listeners.contains(sl) ) {
			listeners.add(sl);
		}
	}

	public void removeCallback(ServerListener sl) {
		if (listeners.contains(sl) ) {
			listeners.remove(sl);
		}
	}
}