package org.libble.linux.interfaces;

public interface Server {
	
	/**
	 * Start the server
	 */
	void start();

	/**
	 * Stop the server
	 */
	void stop();
	
	/**
	 * Restart the server
	 */
	void restartServer();
	
	/**
	 * Flood(ie. send) a message to every connected client
	 * 
	 * @param msg Message, as String
	 */
	default void floodMessage(String msg) {
		floodMessage(msg.getBytes());
	}
	
	/**
	 * Flood(ie. send) a message to every connected client
	 * 
	 * @param msg Message, as byte[]
	 */
	void floodMessage(byte[] msg);
	
	/**
	 * Send message to a specific client
	 * 
	 * @param msg Message, as String
	 * @param addr Client address
	 */
	default void sendMessage(String msg, String addr) {
		sendMessage(msg.getBytes(), addr);
	}
	
	/**
	 * Send message to a specific client
	 * 
	 * @param msg Message, as byte[]
	 * @param addr Client address
	 */
	void sendMessage(byte[] msg, String addr);
	
	/**
	 * Add a callback to be called on different state changes
	 * 
	 */
	public void addCallback(ServerListener sl);
	
	/**
	 * Remove an instance of a callback
	 * 
	 */
	public void removeCallback(ServerListener sl);
}
