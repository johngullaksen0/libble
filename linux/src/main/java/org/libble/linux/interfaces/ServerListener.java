package org.libble.linux.interfaces;

public interface ServerListener {
	void onMessageReceived(byte [] msg, String sender);
	void onClientConnect();
	void onClientDisconnect();
}