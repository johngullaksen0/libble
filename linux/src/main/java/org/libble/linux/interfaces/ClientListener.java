package org.libble.linux.interfaces;

import java.util.List;

public interface ClientListener {
	/**
	 * Called when the client connects to a server
	 */
	void onClientConnect();
	
	/**
	 * Called when the client disconnects from a server
	 */
	void onClientDisconnect();
	
	/**
	 * Called on completion of a device scan 
	 * 
	 * @param devices Found devices 
	 */
	void onScanComplete(List<String> devices);
	
	/**
	 * Called when the client receives a message
	 * 
	 * @param msg Message
	 */
	void onMessageReceived(byte[] msg);
}
