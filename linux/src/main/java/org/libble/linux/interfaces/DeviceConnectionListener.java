package org.libble.linux.interfaces;

import org.bluez.Device1;
import org.libble.linux.bluetooth.LEDevice;

public interface DeviceConnectionListener {
	void onDeviceDiscovered(Device1 dev);
	void onDeviceRemoved(Device1 dev);
}
