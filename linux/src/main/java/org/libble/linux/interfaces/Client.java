package org.libble.linux.interfaces;

public interface Client {
	
	/**
	 * Start scanning for devices
	 */
	void startScan();
	
	/**
	 * Stop scanning for devices
	 */
	void stopScan();
	
	/**
	 * Start scanning for a device with the specific address
	 * 
	 * @param addr Address of device to scan for
	 */
	void startAddressScan(String addr);
	
	/**
	 * Start scanning for a device with the specific device name
	 * 
	 * @param devName Name of device
	 */
	void startDeviceScan(String devName);
	
	/**
	 * Connect to a device
	 *  
	 * @param addr Address of device to connect to
	 */
	void connectDevice(String addr);
	
	/** 
	 * Disconnect from a device, if connected to one
	 */
	void disconnect();
	
	/**
	 * Send a message to the connected server
	 * 
	 * @param msg Message to send, as String
	 */
	default void sendMessage(String msg) {
		sendMessage(msg.getBytes());
	}
	
	/**
	 * Send a message to the connected server
	 * 
	 * @param msg Message to send, as byte[]
	 */
	void sendMessage(byte[] msg);
	
	/**
	 * Get whether we are connected to a server
	 * 
	 * @return connected
	 */
	boolean isConnected();
	
	/**
	 * Get the local device address
	 * 
	 * @return address
	 */
	String getAddress();
	
	/**
	 * Get the address of the remote server we're connected to 
	 * 
	 * @return server address
	 */
	String getServerAddress();
	
	/**
	 * Get whether we are currently scanning for devices
	 * 
	 * @return scanning 
	 */
	boolean isScanning();
	
	/**
	 * Add a callback to be called on different state changes
	 */
	void addCallback(ClientListener cl);
	
	/**
	 * Remove an instance of a callback
	 */
	void removeCallback(ClientListener cl);
	
	//TODO: necessary? can't really see the need in client 
	String getDeviceName();

	// TODO: remove? implementation dependent
	byte[] handleMessage(byte[] msg);
}