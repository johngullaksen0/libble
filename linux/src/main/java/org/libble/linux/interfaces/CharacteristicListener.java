package org.libble.linux.interfaces;

public interface CharacteristicListener {
	public byte[] onRead();
	public void onWrite(byte[] value);
}