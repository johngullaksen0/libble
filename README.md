[![Build Status](https://travis-ci.com/Ebsz/libble.svg?branch=master)](https://travis-ci.com/Ebsz/libble)

# libble

A BLE (Bluetooth Low Energy) library for Java, supporting both Linux and Android.

## Installing

The library can be downloaded as a JAR-file from the project's  [release page](https://github.com/ebsz/libble/releases).

### Gradle

If you are using Gradle, the JAR-file can be added to a module's dependencies simply by modifying the module's `build.gradle`-file:

```groovy
// build.gradle
dependencies {
    ...
    implementation files('path/to/jar/libble.jar')
}
```

When developing for Linux you have to add some additional dependencies:

```groovy
dependencies {
    ...
    compile group: 'com.github.hypfvieh', name: 'dbus-java', version: '3.0.2'
    compile group: 'com.github.hypfvieh', name: 'bluez-dbus', version: '0.1.1'
    compile group: 'org.slf4j', name: 'slf4j-simple', version: '1.7.26'
}
```

When developing for Android you will need Android-libraries. Using Android Studio will help solve this.

### Maven

Using Gradle is the recommended method, but it is also possible to use Maven. To add the JAR-file to your Maven project you first have to install the Maven [command-line interface](https://maven.apache.org/install.html). With the Maven CLI installed you can run the following command:

```shell
$ mvn install:install-file \
   -Dfile=path/to/jar/libble.jar \
   -DgroupId=libble \
   -DartifactId=libble \
   -Dversion=v0.2.1 \
   -Dpackaging=jar \
   -DgeneratePom=true
```

You will also have to add the library as a dependency in your Maven project's pom.xml-file:

```xml
// pom.xml
<dependencies>
    ...
    <dependency>
        <groupId>libble</groupId>
        <artifactId>libble</artifactId>
        <version>v0.2.1</version>
    </dependency>
</dependencies>
```

When developing for Linux you have to add some additional dependencies:

```xml
// pom.xml
<dependencies>
    ...
    <dependency>
        <groupId>libble</groupId>
        <artifactId>libble</artifactId>
        <version>v0.2.1</version>
    </dependency>
    <dependency>
        <groupId>com.github.hypfvieh</groupId>
        <artifactId>dbus-java</artifactId>
        <version>3.0.2</version>
    </dependency>
    <dependency>
        <groupId>com.github.hypfvieh</groupId>
        <artifactId>bluez-dbus</artifactId>
        <version>0.1.1</version>
    </dependency>
    <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-simple</artifactId>
        <version>1.7.26</version>
    </dependency>
</dependencies>
```

When developing for Android you will need Android-libraries. Using Android Studio will help solve this.

## Documentation

You can read the Javadocs [here](https://ebsz.github.io/libble/).
