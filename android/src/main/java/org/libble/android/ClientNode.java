package org.libble.android;

import android.util.Log;

import java.util.List;

import org.libble.android.interfaces.ClientListener;
import org.libble.android.interfaces.NodeListener;
import org.libble.android.interfaces.ServerListener;
import org.libble.android.util.StringUtils;

/**
 * <h1>ClientNode</h1>
 * <p>The ClientNode class implements functionality to connect to a server via BLE. Also is able to function as a server or between a server or another client.</p>
 * <p>It functions in a treelike structure, where on ClientNode can be connected to only one ClientNode, but multiple others can be connected to it.
 * Messages are sent to the top level, where the messages are sent downward the tree.</p>
 * <p>Adding a callback is neccesary for most of the functionality.</p>
 * <p>Currently experimental, meaning unfinished. Expect bugs and non-optimal functionality</p>
 * @see NodeListener
 */
public class ClientNode implements ServerListener, ClientListener {

    BluetoothClient client;
    BluetoothServer server;

    boolean _hosting = false;
    boolean _top_level = false;

    NodeListener appCallback;

    public ClientNode() {

        server = new BluetoothServer();
        server.addCallback(this);
    }

    /**
     * Scans for BLE devices using the service UUID defined in BLEConfig.
     * @return nothing
     * @see org.libble.android.BLEConfig
     */
    public void startScan() {
        if (client != null) {
            client.startScan();
        }
    }

    /**
     * Starts hosting and allows others to connect
     * @return nothing
     */
    public void startHosting() {
        _hosting = true;
        server.start();
    }

    /**
     * Sends a message to the top of the tree.
     * @param msg The message as a String.
     */
   public void sendMessage(String msg) {
        sendMessage(StringUtils.bytesFromString(msg));
   }
    /**
     * Sends a message to the top of the tree.
     * @param msgBytes The message as an array of bytes.
     */
   public void sendMessage(byte[] msgBytes) {
        Log.d("CLIENTNODE", "smd " + _top_level);

        if (_top_level) {
            server.floodMessage(msgBytes);
        }
        else {
            client.sendMessage(msgBytes);
        }
   }
    /**
     * If at the top of the tree, sends a message to a specific address.
     * @param msg The message as an array of bytes.
     */
   public void sendMessage(byte[] msg, String address) {
        if (_top_level) {
            server.sendMessage(msg, address);
        }
    }
    /**
     * Stops hosting the server.
     */
   public void stop() {
        if (_hosting) {
            server.stop();
            _hosting = false;
        }
   }
   /**
    * Disconnects from the server.
    * */
   public void disconnect() {
        if (client != null) {
            client.disconnect();
        }
   }
    /**
     * Removes callbacks and stops the server.
     */
   public void destroy() {
        if (client != null) {
            client.removeCallback(this);
        }
        server.stop();
   }

    @Override
    public void serverMessageCallback(byte[] msg, String sender) {
        if (!_top_level) {
            client.sendMessage(msg);
        }
        else {
            //server.sendMessage(msg);
            if (appCallback != null) {
                appCallback.nodeMessageCallback(msg, sender);
            }
        }
    }

    @Override
    public void serverDisconnectedCallback(String address) {

    }

    @Override
    public void serverConnectedCallback(String address) {

    }

    @Override
    public void clientScanCallback(List<String> bleDevices) {

    }

    @Override
    public void clientMessageCallback(byte[] msg) {
        Log.d("NODE", "CLIENT MESSAGE!");

        if (_hosting) {
            server.floodMessage(msg);
        }
        if (appCallback != null) {
            appCallback.nodeMessageCallback(msg, "");
        }
    }

    @Override
    public void clientConnectedCallback() {
        if (appCallback != null) {
            appCallback.nodeConnectedCallback(client);
        }
    }

    @Override
    public void clientDisconnectedCallback() {
        if (appCallback != null) {
            appCallback.nodeDisconnectedCallback(client);
        }
        _top_level = true;
    }

    /**
     * Sets the specified client as the client used.
     * @param client the client
     */
    public void setClient(BluetoothClient client) {
        this.client = client;
        client.addCallback(this);
    }
    /**
     * Sets the specified callback as callback.
     * @param callback The message as an array of bytes.
     */
    public void setCallback(NodeListener callback) {
        appCallback = callback;
    }
    /**
     * Sets this ClienNode as top level withing the tree.
     */
    public void setTopLevel(boolean value) {
        _top_level = value;

    }

    /**
     * Returns if this object is hosting.
     * @return Boolean
     */
    public boolean isHosting() {
        return _hosting;
    }
    /**
     * Returns if this object is at the top of the tree.
     * @return Boolean
     */
    public boolean isTopLevel() {
        return _top_level;
    }
}
