package org.libble.android;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;

import java.util.UUID;

/**
 * <h1>BLEConfig</h1>
 * <p>Used for initiating the necessary functionality to use BLE on android.</p>
 * <p>Also contains UUIDs used for BLE communications. Change these as the user sees fit.</p>
 * <p>The class is a singleton. Access it statically with the INSTANCE constant.</p>
 *
 */
public final class BLEConfig {
    //Instance of the singleton.
    private static final BLEConfig INSTANCE = new BLEConfig();

    //UUIDs used in BLE communications

    //The service UUID. The UUID used when hosting a server or scanning from a server.
    public static String SERVICE_STRING = "f5a5d298-7521-42c7-9423-f23a615d3861";
    public static UUID SERVICE_UUID = UUID.fromString(SERVICE_STRING);

    //The UUID of the characteristic used for sending messages to the server.
    public static String CHARACTERISTIC_CLIENT_MESSAGE_STRING = "7D2EBAAD-F7BD-485A-BD9D-92AD6ECFE93E";
    public static UUID CHARACTERISTIC_CLIENT_MESSAGE_UUID = UUID.fromString(CHARACTERISTIC_CLIENT_MESSAGE_STRING);

    //The UUID of the characteristic used for sending messages to a client.
    public static String CHARACTERISTIC_SERVER_MESSAGE_STRING = "7D2EDEAD-F7BD-485A-BD9D-92AD6ECFE93E";
    public static UUID CHARACTERISTIC_SERVER_MESSAGE_UUID = UUID.fromString(CHARACTERISTIC_SERVER_MESSAGE_STRING);

    public static String CLIENT_CONFIGURATION_DESCRIPTOR_STRING = "00002902-0000-1000-8000-00805f9b34fb";
    public static UUID CLIENT_CONFIGURATION_DESCRIPTOR_UUID = UUID.fromString(CLIENT_CONFIGURATION_DESCRIPTOR_STRING);

    public static final String CLIENT_CONFIGURATION_DESCRIPTOR_SHORT_ID = "2902";

    public static String BEACON_NAME = "helloUSB";

    public static final long SCAN_PERIOD = 5000;

    private boolean has_been_initiated;
    private Context context;
    private BluetoothManager bluetoothManager;
    private BluetoothAdapter bluetoothAdapter;

    private BLEConfig() {
        has_been_initiated = false;


    }
    /**
     * Initiates the BLE context. Uses a Context variable. Typically an Activity object.
     * @param context
     * @return nothing
     * @see Context
     * @see android.app.Activity
     */
    public void initiate(Context context) {
        this.context = context;
        bluetoothManager = (BluetoothManager) context.getSystemService(context.BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();

        has_been_initiated = true;
    }

    public boolean hasLocationPermissions() {
        if (Build.VERSION.SDK_INT > 22) {
            return context.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        }
        return true;
    }

    public boolean hasPermission() {
        boolean perm = (bluetoothAdapter == null || !bluetoothAdapter.isEnabled());


        return !perm;
    }

    public Context getContext() {
        return context;
    }
    public BluetoothManager getBluetoothManager() { return bluetoothManager; }
    public BluetoothAdapter getBluetoothAdapter() { return bluetoothAdapter; }

    public static BLEConfig getInstance() {
        return INSTANCE;
    }
}
