package org.libble.android.interfaces;

import java.util.List;

import org.libble.android.BluetoothClient;

/**
 * <h1>NodeListener</h1>
 * <p>Used for receiving callbacks from a ClientNode object.</p>
 *
 */
public interface NodeListener {

    /**
     * Called when the client completes scanning for BLE devices.
     * @param bleDevices A list of the addresses of the devices found.
     * @param instance The client object that called the callback.
     * @return nothing
     */
    void nodeScanCallback(List<String> bleDevices, BluetoothClient instance);
    /**
     * Called when the client receives a message.
     * @param msg An array of bytes representing the message.
     * @param sender Is empty when not top level. Otherwise the address of the sender.
     * @return nothing
     */
    void nodeMessageCallback(byte [] msg, String sender);
    /**
     * Called when the client successfully connects to a server.
     * @param instance The client object that called the callback.
     * @return nothing
     */
    void nodeConnectedCallback(BluetoothClient instance);
    /**
     * Called when the client is disconnected from a server.
     * @param instance The client object that called the callback.
     * @return nothing
     */
    void nodeDisconnectedCallback(BluetoothClient instance);
}
