package org.libble.android.interfaces;

import java.util.List;

/**
 * <h1>ClientListener</h1>
 * <p>Used for receiving callbacks from a BluetoothClient object.</p>
 *
 */
public interface ClientListener {

    /**
     * Called when the client completes scanning for BLE devices.
     * @param bleDevices A list of the addresses of the devices found.
     * @return nothing
     */
    void clientScanCallback(List<String> bleDevices);
    /**
     * Called when the client receives a message.
     * @param msg An array of bytes representing the message.
     * @return nothing
     */
    void clientMessageCallback(byte [] msg);
    /**
     * Called when the client successfully connects to a server.
     * @return nothing
     */
    void clientConnectedCallback();
    /**
     * Called when the client is disconnected from a server.
     * @return nothing
     */
    void clientDisconnectedCallback();
}
