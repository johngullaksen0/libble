package org.libble.android.interfaces;

/**
 * <h1>ServerListener</h1>
 * <p>Used for receiving callbacks from a BluetoothServer object.</p>
 *
 */
public interface ServerListener {

    /**
     * Called when the server receives a message.
     * @param msg An array of bytes representing the message.
     * @return nothing
     */
    void serverMessageCallback(byte [] msg, String sender);
    /**
     * Called when a client disconnects.
     * @param address Address of the disconnected client.
     * @return nothing
     */
    void serverDisconnectedCallback(String address);
    /**
     * Called when a client connects.
     * @param address Address of the connected client.
     * @return nothing
     */
    void serverConnectedCallback(String address);
}
