package org.libble.android;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.os.Handler;
import android.os.ParcelUuid;
import android.util.Log;

import org.libble.android.interfaces.Client;
import org.libble.android.interfaces.ClientListener;
import org.libble.android.util.BluetoothUtils;
import org.libble.android.util.ByteUtils;
import org.libble.android.util.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.libble.android.BLEConfig.SCAN_PERIOD;
import static org.libble.android.BLEConfig.SERVICE_UUID;

/**
 * <h1>BluetoothClient</h1>
 * <p>The BluetoothClient class implements functionality to connect to a server via BLE.</p>
 * <p>Adding a callback is neccesary for most of the functionality.</p>
 * @see ClientListener
 */
public class BluetoothClient implements Client {

    private static final String TAG = "BluetoothClient";

    // Constants used for splitting messages.
    public static final byte SPLIT_MESSAGE = 125;
    public static final byte CONNECTION_MESSAGE = 126;

    public static Map<String, BluetoothClient> clients;
    private BluetoothClient instance;
    private String serverAddress;

    private boolean scanning;
    private Handler handler;
    private Map<String, BluetoothDevice> scanResults;
    private Map<String, Integer> rssiResults;

    private boolean connected;
    private boolean characteristicServerMessageInitialized;
    private boolean characteristicClientMessageInitialized;
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothLeScanner bluetoothLeScanner;
    private ScanCallback scanCallback;
    private BluetoothGatt gatt;

    //Buffer used for split messages.
    private Map<Byte, List<Message>> messageBuffer;
    //The current id used for split messages.
    private byte splitId = 0;

    private List<ClientListener> appCallback;

    public BluetoothClient() {
        bluetoothAdapter = BLEConfig.getInstance().getBluetoothAdapter();
        if (clients == null) {
            clients = new HashMap<String, BluetoothClient>();
        }
        instance = this;
        appCallback = new ArrayList<>();

        messageBuffer = Collections.synchronizedMap(new HashMap<>());
    }

    /**
     * Starts scanning for available BLE devices using the service UUID defined in BLEConfig. Calls a callback defined in the addCallback method.
     *
     * @see org.libble.android.BLEConfig
     */
    public void startScan() {
        if (scanning) {
            return;
        }

        disconnect();

        scanResults = new HashMap<>();
        rssiResults = new HashMap<>();
        scanCallback = new BtleScanCallback(scanResults, rssiResults);

        bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();

        ScanFilter scanFilter = new ScanFilter.Builder()
                .setServiceUuid(new ParcelUuid(SERVICE_UUID))
                .build();
        List<ScanFilter> filters = new ArrayList<>();
        filters.add(scanFilter);

        ScanSettings settings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
                .build();

        bluetoothLeScanner.startScan(filters, settings, scanCallback);

        handler = new Handler();
        handler.postDelayed(this::stopScan, SCAN_PERIOD);
        System.out.println(SERVICE_UUID.toString());

        scanning = true;
    }

    /**
     * Stops scanning for BLE devices.
     */
    public void stopScan() {
        if (scanning && bluetoothAdapter != null && bluetoothAdapter.isEnabled() && bluetoothLeScanner != null) {
            bluetoothLeScanner.stopScan(scanCallback);
            scanComplete();
        }

        scanCallback = null;
        scanning = false;
        handler = null;
        log("Stopped scanning.");
    }
    /**
     * Internal function called when the scan is complete.
     */
    private void scanComplete() {
        List<String> results = new ArrayList<String>();
        if (scanResults.isEmpty()) {
            log("no devices found");
            for (ClientListener callback : appCallback) {
                callback.clientScanCallback(results);
            }
            return;
        }

        if (!scanResults.isEmpty()) {
            for (String deviceAddress : scanResults.keySet()) {
                BluetoothDevice device = scanResults.get(deviceAddress);

                results.add(deviceAddress);
            }
        }

        for (ClientListener callback : appCallback) {
            callback.clientScanCallback(results);
        }

        log("Scan completed");
    }
    /**
     * Scans for BLE devices using the address defined in the parameter.
     * @param address The address being searched for.
     * @return nothing
     */
    public void startAddressScan(String address) {
        if (scanning) {
            return;
        }

        disconnect();

        scanResults = new HashMap<>();
        rssiResults = new HashMap<>();
        scanCallback = new BtleScanCallback(scanResults, rssiResults);

        bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();

        ScanFilter scanFilter = new ScanFilter.Builder().setDeviceAddress(address)
                .build();
        List<ScanFilter> filters = new ArrayList<>();
        filters.add(scanFilter);

        ScanSettings settings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
                .build();

        bluetoothLeScanner.startScan(filters, settings, scanCallback);

        handler = new Handler();
        handler.postDelayed(this::stopScan, SCAN_PERIOD);

        scanning = true;
    }
    /**
     * Scans for BLE devices using the device name defined in the parameter.
     * @param deviceName The device name being searched for.
     * @return nothing
     */
    public void startDeviceScan(String deviceName) {
        if (scanning) {
            return;
        }

        disconnect();

        scanResults = new HashMap<>();
        rssiResults = new HashMap<>();
        scanCallback = new BtleScanCallback(scanResults, rssiResults);

        bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();

        ScanFilter scanFilter = new ScanFilter.Builder().setDeviceName(deviceName)
                .build();
        List<ScanFilter> filters = new ArrayList<>();
        filters.add(scanFilter);

        ScanSettings settings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
                .build();

        bluetoothLeScanner.startScan(filters, settings, scanCallback);

        handler = new Handler();
        handler.postDelayed(this::stopScan, SCAN_PERIOD);

        scanning = true;
    }

    /**
     * Connects to the device whose address is defined. Must scan before connecting.
     * @param address The address of the device being connected to.
     * @return nothing
     */
    public void connectDevice(String address) {
        if (!scanResults.containsKey(address)) {
            log("Cannot find " + address);
            return;
        }

        serverAddress = address;
        if (isConnected()) {
            disconnect();
        }
        BluetoothDevice device = scanResults.get(address);

        log("Connecting to " + device.getAddress());
        GattClientCallback gattClientCallback = new GattClientCallback();
        gatt = device.connectGatt(BLEConfig.getInstance().getContext(), false, gattClientCallback);
        System.out.println(gatt);
    }

    /**
     * Sends a message to the server connected to the client.
     * @param msgBytes The message being sent. In byte array.
     * @return nothing
     */
    public void sendMessage(byte[] msgBytes) {
        int maxLength = 20; // The maximal size in bytes of a single message.

        //Checks if the message is too large. If it is, it splits it into smaller messages.
        if (msgBytes.length > maxLength) {
            byte[] msgData = {SPLIT_MESSAGE, 0, 0, splitId}; // 0: The type of message. 1: The amount of messages that is part of the complete message. 2: The index of the current message. 3: The unique id of the whole message.
            maxLength -= msgData.length;
            double nm = ((double)msgBytes.length) / ((double)maxLength);
            byte numMsg = (byte)Math.ceil(nm);
            msgData[1] = numMsg;

            log("Sending split message, length: " + numMsg + " : " + nm);
            for (byte i = 0; i < numMsg; i++) { // Sending each message.
                byte[] splitMessage = ByteUtils.subArray(msgBytes, i * maxLength, i * maxLength + maxLength);
                msgData[2] = i;
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                log(StringUtils.stringFromBytes(splitMessage));
                sendData(ByteUtils.joinArrays(msgData, splitMessage));
            }

            splitId++;
            if (splitId > 100) {
                splitId = 0;
            }
        }
        else {
            sendData(msgBytes);
        }
    }
    /**
     * Called when the recieving a message. Meant to be possibly overridden in subclasses.
     * @param msgBytes The message recieved.
     * @return array of bytes. Usually the message recieved.
     */
    public synchronized byte[] handleMessage(byte[] msgBytes) {
        //Checks if the message is part of a bigger message. And puts it together.
        if (msgBytes[0] == BluetoothServer.SPLIT_MESSAGE) {
            List<Message> msgList;
            if (messageBuffer.containsKey(msgBytes[3])) {
                msgList = messageBuffer.get(msgBytes[3]);
            }
            else {
                msgList = new ArrayList<>();
                for (byte i = 0; i < msgBytes[1]; i++) {
                    msgList.add(null);
                }
                messageBuffer.put(msgBytes[3], msgList);
            }
            msgList.set(msgBytes[2], new Message(ByteUtils.subArray(msgBytes, 4)));

            boolean msgReady = true;
            for (Message m : msgList) {
                if (m == null) {
                    msgReady = false;
                }
            }
            if (msgReady) {
                byte[] finalMsg = {};
                for (Message ms : msgList) {
                    finalMsg = ByteUtils.joinArrays(finalMsg, ms.messageData);
                }

                messageBuffer.remove(msgBytes[3]);
                for (ClientListener callback : appCallback) {
                    callback.clientMessageCallback(finalMsg);
                }
            }

        }
        else {

            for (ClientListener callback : appCallback) {
                callback.clientMessageCallback(msgBytes);
            }

        }

        return msgBytes;
    }

    /**
     * Internal method for sending message.
     * @param message The message being sent. In byte array.
     * @return nothing
     */
    private void sendData(byte[] message) {
        if (!connected || !characteristicClientMessageInitialized) {
            return;
        }

        BluetoothGattCharacteristic characteristic = BluetoothUtils.findClientMessageCharacteristic(gatt);
        if (characteristic == null) {
            logError("Unable to find client message characteristic.");
            disconnect();
            return;
        }

        byte[] messageBytes = message;
        if (messageBytes.length == 0) {
            logError("Unable to convert message to bytes");
            return;
        }

        characteristic.setValue(messageBytes);
        boolean success = gatt.writeCharacteristic(characteristic);
        if (success) {
            log("Wrote: " + StringUtils.byteArrayInHexFormat(messageBytes));
        } else {
            logError("Failed to write data");
        }
    }

    /**
     * A class representing a single message. Used internally.
     */
    class Message {
        byte[] messageData;
        public Message(byte[] messageData) {
            this.messageData = messageData;
        }
    }

    private void logError(String msg) {
        log("Error: " + msg);
    }

    private void setConnected(boolean connected) {
        this.connected = connected;
    }

    private void initializeServerMessageCharacteristic() {
        characteristicServerMessageInitialized = true;
    }

    private void initializeClientMessageCharacteristic() {
        characteristicClientMessageInitialized = true;
    }

    /**
     * Disconnects from the server, if connected.
     * @return nothing
     */
    public void disconnect() {
        if (!connected) {
            return;
        }
        log("Closing Gatt connection");
        connected = false;
        characteristicClientMessageInitialized = false;
        characteristicServerMessageInitialized = false;
        if (gatt != null) {
            gatt.disconnect();
            gatt.close();
        }
        if (appCallback != null) {

            for (ClientListener callback : appCallback) {
                callback.clientDisconnectedCallback();
            }
        }
    }

    private void log(String m) {
        Log.d(TAG, m);
    }

    /**
     * Gets the name of the BluetoothAdapter
     * @return A String representing the name of the BluetoothAdapter.
     * @see BluetoothAdapter
     */
    public String getName() {
        return bluetoothAdapter.getName();
    }
    /**
     * Gets the address of the BluetoothAdapter.
     * @return String of the Address.
     * @see BluetoothAdapter
     */
    public String getAddress() { return bluetoothAdapter.getAddress(); }
    /**
     * Gets the BluetoothDevice with the specified address.
     * @param address String of the address
     * @return BluetoothDevice object.
     */
    public BluetoothDevice getDevice (String address) {return bluetoothAdapter.getRemoteDevice(address);}
    /**
     * Gets the address of the server.
     * @return String of the address.
     */
    public String getServerAddress() {
        return serverAddress;
    }
    /**
     * Gets the name of the device of the specified address.
     * @param address The message being sent. In byte array.
     * @return String of the device's name.
     */
    public String getDeviceName(String address) {
        if (scanResults.get(address) != null) {
            return scanResults.get(address).getName();
        }
        return "0";
    }
    /**
     * Gets the rssi of the found devices;
     * @return String of the device's name.
     */
    public Map<String, Integer> getRssi() {
        return rssiResults;
    }

    /**
     * Gets if the client is currently scanning for BLE devices.
     * @return Boolean
     */
    public boolean isScanning() {
        return scanning;
    }
    /**
     * Gets if the client is currently connected.
     * @return Boolean
     */
    public boolean isConnected() {
        return connected;
    }
    /**
     * Adds a callback. The callback is called when changes occur in the client.
     * Such as results of scanning, receiving messages or connection changes.
     * @param callback The callback
     * @return nothing
     */
    public void addCallback(ClientListener callback) {
        appCallback.add(callback);
    }
    /**
     * Removes a callback. The callback is called when changes occur in the client.
     * Such as results of scanning, receiving messages or connection changes.
     * @param callback The callback
     * @return nothing
     */
    public void removeCallback(ClientListener callback) {
        appCallback.remove(callback);
    }

    @Override
    public String getDeviceName() {
        return null;
    }

    /**
     * <h1>BtleScanCallback</h1>
     * <p>Internal class used for callbacks when scanning for BLE devices.</p>
     */
    private class BtleScanCallback extends ScanCallback {

        private Map<String, BluetoothDevice> mScanResults;
        private Map<String, Integer> mRssiResults;


        BtleScanCallback(Map<String, BluetoothDevice> scanResults, Map<String, Integer> rssiResults) {
            mScanResults = scanResults;
            mRssiResults = rssiResults;

        }

        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            addScanResult(result);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            for (ScanResult result : results) {
                addScanResult(result);
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            logError("BLE Scan Failed with code " + errorCode);
        }

        private void addScanResult(ScanResult result) {
            BluetoothDevice device = result.getDevice();
            String deviceAddress = device.getAddress();
            Integer rssi = result.getRssi();
            mScanResults.put(deviceAddress, device);
            if (mRssiResults != null){
                mRssiResults.put(deviceAddress, rssi);
            }
            else {
                System.out.println("no rssi value found");

            }
        }
    }

    /**
     * <h1>GattClientCallback</h1>
     * <p>Internal used for callbacks relating to being a Gatt client in BLE. </p>
     */
    private class GattClientCallback extends BluetoothGattCallback {

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);
            log("onConnectionStateChange newState: " + newState);

            if (status == BluetoothGatt.GATT_FAILURE) {
                logError("Connection Gatt failure status " + status);
                disconnect();
                return;
            } else if (status != BluetoothGatt.GATT_SUCCESS) {
                // handle anything not SUCCESS as failure
                logError("Connection not GATT sucess status " + status);
                disconnect();
                return;
            }

            if (newState == BluetoothProfile.STATE_CONNECTED) {
                log("Connected to device " + gatt.getDevice().getAddress());
                setConnected(true);
                String address = gatt.getDevice().getAddress();
                serverAddress = address;
                if (!clients.containsKey(address)) {
                    clients.put(address, instance);
                }
                gatt.discoverServices();
                if (appCallback != null) {

                    for (ClientListener callback : appCallback) {
                        callback.clientConnectedCallback();
                    }
                }
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                log("Disconnected from device");
                String address = gatt.getDevice().getAddress();
                if (clients.containsKey(address)) {
                    clients.remove(address);
                }
                disconnect();
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);

            if (status != BluetoothGatt.GATT_SUCCESS) {
                log("Device service discovery unsuccessful, status " + status);
                return;
            }

            //BluetoothUtils.findClientMessageCharacteristic(gatt);

            List<BluetoothGattCharacteristic> matchingCharacteristics = BluetoothUtils.findCharacteristics(gatt);
            if (matchingCharacteristics.isEmpty()) {
                logError("Unable to find characteristics.");
                return;
            }

            log("Initializing: setting write type and enabling notification");
            for (BluetoothGattCharacteristic characteristic : matchingCharacteristics) {
                characteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                enableCharacteristicNotification(gatt, characteristic);
            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                log("Characteristic written successfully");
            } else {
                logError("Characteristic write unsuccessful, status: " + status);
                disconnect();
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                log("Characteristic read successfully");
                readCharacteristic(characteristic);
            } else {
                logError("Characteristic read unsuccessful, status: " + status);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            log("Characteristic changed, " + characteristic.getUuid().toString());
            readCharacteristic(characteristic);
        }


        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                log("Descriptor written successfully: " + descriptor.getUuid().toString());
                initializeServerMessageCharacteristic();
            } else {
                logError("Descriptor write unsuccessful: " + descriptor.getUuid().toString());
            }
        }


        private void enableCharacteristicNotification(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            boolean characteristicWriteSuccess = gatt.setCharacteristicNotification(characteristic, true);
            if (characteristicWriteSuccess) {
                log("Characteristic notification set successfully for " + characteristic.getUuid().toString());
                if (BluetoothUtils.isClientMessageCharacteristic(characteristic)) {
                    initializeClientMessageCharacteristic();
                } else if (BluetoothUtils.isServerMessageCharacteristic(characteristic)) {
                    enableCharacteristicConfigurationDescriptor(gatt, characteristic);
                }
            } else {
                logError("Characteristic notification set failure for " + characteristic.getUuid().toString());
            }
        }

        private void enableCharacteristicConfigurationDescriptor(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {

            List<BluetoothGattDescriptor> descriptorList = characteristic.getDescriptors();
            BluetoothGattDescriptor descriptor = BluetoothUtils.findClientConfigurationDescriptor(descriptorList);
            if (descriptor == null) {
                logError("Unable to find Characteristic Configuration Descriptor");
                return;
            }

            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            boolean descriptorWriteInitiated = gatt.writeDescriptor(descriptor);
            if (descriptorWriteInitiated) {
                log("Characteristic Configuration Descriptor write initiated: " + descriptor.getUuid().toString());
            } else {
                logError("Characteristic Configuration Descriptor write failed to initiate: " + descriptor.getUuid().toString());
            }
        }

        private void readCharacteristic(BluetoothGattCharacteristic characteristic) {
            byte[] messageBytes = characteristic.getValue();
            log("Read: " + StringUtils.byteArrayInHexFormat(messageBytes));
            String message = StringUtils.stringFromBytes(messageBytes);
            if (message == null) {
                logError("Unable to convert bytes to string");
                return;
            }

            handleMessage(messageBytes);
            //appCallback.clientMessageCallback(messageBytes, instance);
            log("Received message: " + message);
        }
    }
}
