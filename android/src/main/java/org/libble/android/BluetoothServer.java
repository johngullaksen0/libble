package org.libble.android;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.os.Handler;
import android.os.ParcelUuid;
import android.util.Log;

import org.libble.android.interfaces.Server;
import org.libble.android.interfaces.ServerListener;
import org.libble.android.util.BluetoothUtils;
import org.libble.android.util.ByteUtils;
import org.libble.android.util.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.libble.android.BLEConfig.CHARACTERISTIC_CLIENT_MESSAGE_UUID;
import static org.libble.android.BLEConfig.CHARACTERISTIC_SERVER_MESSAGE_UUID;
import static org.libble.android.BLEConfig.CLIENT_CONFIGURATION_DESCRIPTOR_UUID;
import static org.libble.android.BLEConfig.SERVICE_UUID;

/**
 * <h1>BluetoothServer</h1>
 * <p>The BluetoothServer class implements functionality required to act as a server using BLE.</p>
 * <p>Adding a callback is neccesary for most of the functionality.</p>
 * @see ServerListener
 */
public class BluetoothServer implements Server {

    private static final String TAG = "BluetoothServer";


    public static final byte SPLIT_MESSAGE = 125;
    public static final byte CONNECTION_MESSAGE = 126;

    private Handler mHandler;
    private List<BluetoothDevice> mDevices;
    private Map<String, String> deviceNames;
    private Map<String, byte[]> mClientConfigurations;

    private BluetoothGattServer mGattServer;
    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeAdvertiser mBluetoothLeAdvertiser;

    private List<ServerListener> appCallback;

    private Map<Byte, List<Message>> messageBuffer;
    private byte splitId = 0;


    public BluetoothServer() {
        mHandler = new Handler();
        mDevices = new ArrayList<>();
        mClientConfigurations = new HashMap<>();

        deviceNames = new HashMap<String, String>();

        mBluetoothManager = BLEConfig.getInstance().getBluetoothManager();
        mBluetoothAdapter = BLEConfig.getInstance().getBluetoothAdapter();

        messageBuffer = Collections.synchronizedMap(new HashMap<>());
        appCallback = new ArrayList<>();
    }

    /**
     * Start the server.
     */
    public void start() {
        if (!mBluetoothAdapter.isMultipleAdvertisementSupported()) {
            log("No Advertising Support.");
            return;
        }

        mBluetoothLeAdvertiser = mBluetoothAdapter.getBluetoothLeAdvertiser();
        GattServerCallback gattServerCallback = new GattServerCallback();
        mGattServer = mBluetoothManager.openGattServer(BLEConfig.getInstance().getContext(), gattServerCallback);

        setupServer();
        startAdvertising();
    }
    /**
     * Stops server.
     */
    public void stop() {
        stopAdvertising();
        stopServer();
    }

    /**
     * Sends a message to every client connected to the server.
     * @param msg The message as a String.
     * @return nothing
     */
    public void floodMessage(String msg) {
        floodMessage(StringUtils.bytesFromString(msg));
    }
    /**
     * Sends a message to every client connected to the server.
     * @param msgBytes The message as an array of bytes.
     * @return nothing
     */
    public void floodMessage(byte[] msgBytes) {
        int maxLength = 20;

        if (msgBytes.length > maxLength) {
            byte[] msgData = {SPLIT_MESSAGE, 0, 0, splitId};
            maxLength -= msgData.length;
            double nm = ((double)msgBytes.length) / ((double)maxLength);
            byte numMsg = (byte)Math.ceil(nm);
            msgData[1] = numMsg;

            log("Sending split message, length: " + numMsg + " : " + nm);
            for (byte i = 0; i < numMsg; i++) {
                byte[] splitMessage = {0, 0};
                splitMessage = ByteUtils.subArray(msgBytes, i * maxLength, i * maxLength + maxLength);
                msgData[2] = i;
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                notifyCharacteristic(ByteUtils.joinArrays(msgData, splitMessage));
            }

            splitId++;
            if (splitId > 100) {
                splitId = 0;
            }
        }
        else {
            notifyCharacteristic(msgBytes);
        }
    }
    /**
     * Sends a message to the client speciefied.
     * @param msg The message as a String.
     * @param address The address of the client.
     * @return nothing
     */
    public void sendMessage(String msg, String address) {
        sendMessage(StringUtils.bytesFromString(msg), address);
    }
    /**
     * Sends a message to the client speciefied.
     * @param msgBytes The message as an array of bytes.
     * @param address The address of the client.
     * @return nothing
     */
    public void sendMessage(byte[] msgBytes, String address) {
        int maxLength = 20;

        if (msgBytes.length > maxLength) {
            byte[] msgData = {SPLIT_MESSAGE, 0, 0, splitId};
            maxLength -= msgData.length;
            double nm = ((double)msgBytes.length) / ((double)maxLength);
            byte numMsg = (byte)Math.ceil(nm);
            msgData[1] = numMsg;

            log("Sending split message, length: " + numMsg + " : " + nm);
            for (byte i = 0; i < numMsg; i++) {
                byte[] splitMessage = {0, 0};
                splitMessage = ByteUtils.subArray(msgBytes, i * maxLength, i * maxLength + maxLength);
                msgData[2] = i;
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                notifyCharacteristic(ByteUtils.joinArrays(msgData, splitMessage), CHARACTERISTIC_SERVER_MESSAGE_UUID, address);
            }

            splitId++;
            if (splitId > 100) {
                splitId = 0;
            }
        }
        else {
            notifyCharacteristic(msgBytes, CHARACTERISTIC_SERVER_MESSAGE_UUID, address);
        }
    }
    /**
     * Called when recieving a message from a client. Meant to be possibly overridden in subclasses.
     * @param msgBytes The message as an array of bytes.
     * @param sender The address of the sender.
     * @return nothing
     */
    protected byte[] handleMessage(byte[] msgBytes, String sender) {
        if (msgBytes[0] == BluetoothServer.SPLIT_MESSAGE) {
            List<Message> msgList;
            if (messageBuffer.containsKey(msgBytes[3])) {
                msgList = messageBuffer.get(msgBytes[3]);
            }
            else {
                msgList = new ArrayList<>();
                for (byte i = 0; i < msgBytes[1]; i++) {
                    msgList.add(null);
                }
                messageBuffer.put(msgBytes[3], msgList);
            }
            msgList.set(msgBytes[2], new Message(ByteUtils.subArray(msgBytes, 4)));

            boolean msgReady = true;
            for (Message m : msgList) {
                if (m == null) {
                    msgReady = false;
                }
            }
            if (msgReady) {
                byte[] finalMsg = {};
                for (Message ms : msgList) {
                    finalMsg = ByteUtils.joinArrays(finalMsg, ms.messageData);
                }

                messageBuffer.remove(msgBytes[3]);
                for (ServerListener callback : appCallback) {
                    callback.serverMessageCallback(finalMsg, sender);
                }
            }

        }
        else if (msgBytes[0] == CONNECTION_MESSAGE){
            //Connection is open.
            byte[] message = {CONNECTION_MESSAGE};
            sendMessage(message, sender);
        } else {
            for (ServerListener callback : appCallback) {
                callback.serverMessageCallback(msgBytes, sender);
            }
        }

        return msgBytes;
    }
    /**
     * A class representing a single message. Used internally.
     */
    class Message {
        byte[] messageData;
        public Message(byte[] messageData) {
            this.messageData = messageData;
        }
    }

    /**
     * Sets up the server.
     * @return nothing
     */
    private void setupServer() {
        BluetoothGattService service = new BluetoothGattService(SERVICE_UUID,
                BluetoothGattService.SERVICE_TYPE_PRIMARY);

        // Write characteristic
        BluetoothGattCharacteristic writeCharacteristic = new BluetoothGattCharacteristic(
                CHARACTERISTIC_CLIENT_MESSAGE_UUID,
                BluetoothGattCharacteristic.PROPERTY_WRITE,
                BluetoothGattCharacteristic.PERMISSION_WRITE);

        // Characteristic with Descriptor
        BluetoothGattCharacteristic notifyCharacteristic = new BluetoothGattCharacteristic(
                CHARACTERISTIC_SERVER_MESSAGE_UUID,
                0,
                0);

        BluetoothGattDescriptor clientConfigurationDescriptor = new BluetoothGattDescriptor(
                CLIENT_CONFIGURATION_DESCRIPTOR_UUID,
                BluetoothGattDescriptor.PERMISSION_READ
                        | BluetoothGattDescriptor.PERMISSION_WRITE);
        clientConfigurationDescriptor.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);

        notifyCharacteristic.addDescriptor(clientConfigurationDescriptor);

        service.addCharacteristic(writeCharacteristic);
        service.addCharacteristic(notifyCharacteristic);

        mGattServer.addService(service);
    }
    /**
     * Stops the server.
     * @return nothing
     */
    public void stopServer() {
        if (mGattServer != null) {
            mGattServer.close();
        }
    }

    /**
     * Stops and starts the server.
     * @return nothing
     */
    public void restartServer() {
        stopAdvertising();
        stopServer();
        setupServer();
        startAdvertising();
    }

    /**
     * Starts advertising. Allows clients to connect to the server.
     * @return nothing
     */
    private void startAdvertising() {
        if (mBluetoothLeAdvertiser == null) {
            return;
        }

        AdvertiseSettings settings = new AdvertiseSettings.Builder()
                .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_BALANCED)
                .setConnectable(true)
                .setTimeout(0)
                .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_LOW)
                .build();

        ParcelUuid parcelUuid = new ParcelUuid(SERVICE_UUID);
        AdvertiseData data = new AdvertiseData.Builder()
                .setIncludeDeviceName(false)
                .addServiceUuid(parcelUuid)
                .build();
        AdvertiseData mScanResponseData = new AdvertiseData.Builder()
                .setIncludeDeviceName(true)
                .build();

        mBluetoothLeAdvertiser.startAdvertising(settings, data,mScanResponseData, mAdvertiseCallback);
    }

    /**
     * Stops advertising. Stops allowing clients to connect to the server.
     * @return nothing
     */
    private void stopAdvertising() {
        if (mBluetoothLeAdvertiser != null) {
            mBluetoothLeAdvertiser.stopAdvertising(mAdvertiseCallback);
        }
    }

    /**
     * Internal class used for starting Advertising.
     */
    private AdvertiseCallback mAdvertiseCallback = new AdvertiseCallback() {
        @Override
        public void onStartSuccess(AdvertiseSettings settingsInEffect) {
            log("Peripheral advertising started.");
        }

        @Override
        public void onStartFailure(int errorCode) {
            log("Peripheral advertising failed: " + errorCode);
        }
    };

    private void notifyCharacteristic(byte[] value) {
        notifyCharacteristic(value, CHARACTERISTIC_SERVER_MESSAGE_UUID);
    }

    private void notifyCharacteristic(byte[] value, UUID uuid) {
        mHandler.post(() -> {
            BluetoothGattService service = mGattServer.getService(SERVICE_UUID);
            BluetoothGattCharacteristic characteristic = service.getCharacteristic(uuid);
            log("Notifying characteristic " + characteristic.getUuid().toString()
                    + ", new value: " + StringUtils.byteArrayInHexFormat(value));

            characteristic.setValue(value);
            // Indications require confirmation, notifications do not
            boolean confirm = BluetoothUtils.requiresConfirmation(characteristic);
            for (BluetoothDevice device : mDevices) {
                if (clientEnabledNotifications(device, characteristic)) {
                    mGattServer.notifyCharacteristicChanged(device, characteristic, confirm);
                }
            }
        });
    }
    private void notifyCharacteristic(byte[] value, UUID uuid, String sender) {
        mHandler.post(() -> {
            BluetoothGattService service = mGattServer.getService(SERVICE_UUID);
            BluetoothGattCharacteristic characteristic = service.getCharacteristic(uuid);
            log("Notifying characteristic " + characteristic.getUuid().toString()
                    + ", new value: " + StringUtils.byteArrayInHexFormat(value));

            characteristic.setValue(value);
            // Indications require confirmation, notifications do not
            boolean confirm = BluetoothUtils.requiresConfirmation(characteristic);
            for (BluetoothDevice device : mDevices) {
                if (clientEnabledNotifications(device, characteristic) && device.getAddress().equals(sender)) {
                    mGattServer.notifyCharacteristicChanged(device, characteristic, confirm);
                }
            }
        });
    }

    private boolean clientEnabledNotifications(BluetoothDevice device, BluetoothGattCharacteristic characteristic) {
        List<BluetoothGattDescriptor> descriptorList = characteristic.getDescriptors();
        BluetoothGattDescriptor descriptor = BluetoothUtils.findClientConfigurationDescriptor(descriptorList);
        if (descriptor == null) {
            // There is no client configuration descriptor, treat as true
            return true;
        }
        String deviceAddress = device.getAddress();
        byte[] clientConfiguration = mClientConfigurations.get(deviceAddress);
        if (clientConfiguration == null) {
            // Descriptor has not been set
            return false;
        }

        byte[] notificationEnabled = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE;
        return clientConfiguration.length == notificationEnabled.length
                && (clientConfiguration[0] & notificationEnabled[0]) == notificationEnabled[0]
                && (clientConfiguration[1] & notificationEnabled[1]) == notificationEnabled[1];
    }



    private void addDevice(BluetoothDevice device) {
        log("Deviced added: " + device.getAddress());

        for (ServerListener callback : appCallback) {
            callback.serverConnectedCallback(device.getAddress());
        }
        mHandler.post(() -> mDevices.add(device));
        deviceNames.put(device.getAddress(), device.getAddress());
    }

    private void removeDevice(BluetoothDevice device) {
        log("Deviced removed: " + device.getAddress());
        mHandler.post(() -> {
            mDevices.remove(device);
            String deviceAddress = device.getAddress();
            mClientConfigurations.remove(deviceAddress);
        });
        deviceNames.remove(device.getAddress());
    }

    private void addClientConfiguration(BluetoothDevice device, byte[] value) {
        String deviceAddress = device.getAddress();
        mClientConfigurations.put(deviceAddress, value);
    }

    private void sendResponse(BluetoothDevice device, int requestId, int status, int offset, byte[] value) {
        mGattServer.sendResponse(device, requestId, status, 0, null);
    }

    /**
     * Gets the name of the BluetoothAdapter.
     * @return String of the name.
     * @see BluetoothAdapter
     */
    public String getName() {
        return mBluetoothAdapter.getName();
    }
    /**
     * Gets the address of the BluetoothAdapter.
     * @return String of the Address.
     * @see BluetoothAdapter
     */
    public String getAddress() {
        return mBluetoothAdapter.getAddress();
    }

    /**
     * Adds a callback. The callback is called when changes occur in the server.
     * Such as results of receiving messages or connection changes.
     * @param callback The callback
     * @return nothing
     */
    public void addCallback(ServerListener callback) {
        appCallback.add(callback);
    }
    /**
     * Removes a callback. The callback is called when changes occur in the server.
     * Such as results of receiving messages or connection changes.
     * @param callback The callback
     * @return nothing
     */
    public void removeCallback(ServerListener callback) {
        appCallback.add(callback);
    }


    private void log(String m) {
        Log.d(TAG, m);
    }

    /**
     * <h1>GattServerCallback</h1>
     * <p>Internal class used as a callback when interacting with the Android BLE API.</p>
     */
    private class GattServerCallback extends BluetoothGattServerCallback {

        @Override
        public void onConnectionStateChange(BluetoothDevice device, int status, int newState) {
            super.onConnectionStateChange(device, status, newState);
            log("onConnectionStateChange " + device.getAddress()
                    + "\nstatus " + status
                    + "\nnewState " + newState);

            if (newState == BluetoothProfile.STATE_CONNECTED) {
                addDevice(device);
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {

                for (ServerListener callback : appCallback) {
                    callback.serverDisconnectedCallback(device.getAddress());
                }
                removeDevice(device);
            }
        }

        @Override
        public void onCharacteristicReadRequest(BluetoothDevice device,
                                                int requestId,
                                                int offset,
                                                BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicReadRequest(device, requestId, offset, characteristic);

            log("onCharacteristicReadRequest " + characteristic.getUuid().toString());

            if (BluetoothUtils.requiresResponse(characteristic)) {
                // Unknown read characteristic requiring response, send failure
                sendResponse(device, requestId, BluetoothGatt.GATT_FAILURE, 0, null);
            }
        }

        @Override
        public void onCharacteristicWriteRequest(BluetoothDevice device,
                                                 int requestId,
                                                 BluetoothGattCharacteristic characteristic,
                                                 boolean preparedWrite,
                                                 boolean responseNeeded,
                                                 int offset,
                                                 byte[] value) {
            super.onCharacteristicWriteRequest(device,
                    requestId,
                    characteristic,
                    preparedWrite,
                    responseNeeded,
                    offset,
                    value);
            log("onCharacteristicWriteRequest " + characteristic.getUuid().toString()
                    + "\nReceived: " + StringUtils.byteArrayInHexFormat(value));
            value = handleMessage(value, device.getAddress());

            if (CHARACTERISTIC_CLIENT_MESSAGE_UUID.equals(characteristic.getUuid())) {
                sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, null);
            }
        }

        @Override
        public void onDescriptorReadRequest(BluetoothDevice device,
                                            int requestId,
                                            int offset,
                                            BluetoothGattDescriptor descriptor) {
            super.onDescriptorReadRequest(device, requestId, offset, descriptor);
            log("onDescriptorReadRequest" + descriptor.getUuid().toString());
        }

        @Override
        public void onDescriptorWriteRequest(BluetoothDevice device,
                                             int requestId,
                                             BluetoothGattDescriptor descriptor,
                                             boolean preparedWrite,
                                             boolean responseNeeded,
                                             int offset,
                                             byte[] value) {
            super.onDescriptorWriteRequest(device, requestId, descriptor, preparedWrite, responseNeeded, offset, value);
            log("onDescriptorWriteRequest: " + descriptor.getUuid().toString()
                    + "\nvalue: " + StringUtils.byteArrayInHexFormat(value));

            if (CLIENT_CONFIGURATION_DESCRIPTOR_UUID.equals(descriptor.getUuid())) {
                addClientConfiguration(device, value);
                sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, null);
            }
        }

        @Override
        public void onNotificationSent(BluetoothDevice device, int status) {
            super.onNotificationSent(device, status);
            log("onNotificationSent");
        }
    }
}
