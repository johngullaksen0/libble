package org.libble.android.util;

import java.util.Arrays;

public class ByteUtils {

    public static byte[] reverse(byte[] value) {
        int length = value.length;
        byte[] reversed = new byte[length];
        for (int i = 0; i < length; i++) {
            reversed[i] = value[length - (i + 1)];
        }
        return reversed;
    }

    public static byte [] joinArrays(byte[] ar1, byte[] ar2) {
        byte[] res = new byte[ar1.length + ar2.length];
        System.arraycopy(ar1, 0, res, 0, ar1.length);
        System.arraycopy(ar2, 0, res, ar1.length, ar2.length);
        return res;
    }
    public static byte [] subArray(byte[] ar1, int src) {
        return subArray(ar1, src, ar1.length);
    }
    public static byte [] subArray(byte[] ar1, int src, int end) {

        if (end > ar1.length) end = ar1.length;

        byte[] res = Arrays.copyOfRange(ar1, src, end);
        return res;
    }
}
