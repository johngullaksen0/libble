package org.libble.android.util;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.libble.android.BLEConfig.CHARACTERISTIC_CLIENT_MESSAGE_STRING;
import static org.libble.android.BLEConfig.CHARACTERISTIC_SERVER_MESSAGE_STRING;
import static org.libble.android.BLEConfig.CLIENT_CONFIGURATION_DESCRIPTOR_SHORT_ID;
import static org.libble.android.BLEConfig.SERVICE_STRING;

public class BluetoothUtils {

    // Characteristics
    /**
     * Finds the ServerMessage and ClientMessage characteristics.
     * @param bluetoothGatt The Gatt context
     * @return A list of Characteristics
     */
    public static List<BluetoothGattCharacteristic> findCharacteristics(BluetoothGatt bluetoothGatt) {
        List<BluetoothGattCharacteristic> matchingCharacteristics = new ArrayList<>();

        List<BluetoothGattService> serviceList = bluetoothGatt.getServices();
        BluetoothGattService service = BluetoothUtils.findService(serviceList);
        if (service == null) {
            return matchingCharacteristics;
        }

        List<BluetoothGattCharacteristic> characteristicList = service.getCharacteristics();
        for (BluetoothGattCharacteristic characteristic : characteristicList) {
            if (isMatchingCharacteristic(characteristic)) {
                matchingCharacteristics.add(characteristic);
            }
        }

        return matchingCharacteristics;
    }

    /**
     * Finds the ClientMessage characteristic.
     * @param bluetoothGatt The Gatt context
     * @return BLE Gatt Characteristic
     */
    @Nullable
    public static BluetoothGattCharacteristic findClientMessageCharacteristic(BluetoothGatt bluetoothGatt) {
        return findCharacteristic(bluetoothGatt, CHARACTERISTIC_CLIENT_MESSAGE_STRING);
    }

    /**
     * Finds the ServerMessage characteristic.
     * @param bluetoothGatt The Gatt context
     * @return BLE Gatt Characteristic
     */
    @Nullable
    public static BluetoothGattCharacteristic findServerMessageCharacteristic(BluetoothGatt bluetoothGatt) {
        return findCharacteristic(bluetoothGatt, CHARACTERISTIC_SERVER_MESSAGE_STRING);
    }

    /**
     * Finds the specified characteristic.
     * @param bluetoothGatt The Gatt context
     * @param uuidString The UUID of the characteristic as a string.
     * @return BLE Gatt Characteristic
     */
    @Nullable
    private static BluetoothGattCharacteristic findCharacteristic(BluetoothGatt bluetoothGatt, String uuidString) {
        List<BluetoothGattService> serviceList = bluetoothGatt.getServices();
        BluetoothGattService service = BluetoothUtils.findService(serviceList);
        if (service == null) {
            return null;
        }

        List<BluetoothGattCharacteristic> characteristicList = service.getCharacteristics();
        for (BluetoothGattCharacteristic characteristic : characteristicList) {
            if (characteristicMatches(characteristic, uuidString)) {
                return characteristic;
            }
        }

        return null;
    }

    /**
     * Finds out if the specified characteristic is the ClientMessage characteristic.
     * @param characteristic The specified Characteristic
     * @return Boolean
     */
    public static boolean isClientMessageCharacteristic(BluetoothGattCharacteristic characteristic) {
        return characteristicMatches(characteristic, CHARACTERISTIC_CLIENT_MESSAGE_STRING);
    }

    /**
     * Finds out if the specified characteristic is the ServerMessage characteristic.
     * @param characteristic The specified Characteristic
     * @return Boolean
     */
    public static boolean isServerMessageCharacteristic(BluetoothGattCharacteristic characteristic) {
        return characteristicMatches(characteristic, CHARACTERISTIC_SERVER_MESSAGE_STRING);
    }

    /**
     * Finds out if the specified characteristic matches with the specified UUID.
     * @param characteristic The specified Characteristic
     * @param uuidString The specified UUID as a string.
     * @return Boolean
     */
    private static boolean characteristicMatches(BluetoothGattCharacteristic characteristic, String uuidString) {
        if (characteristic == null) {
            return false;
        }
        UUID uuid = characteristic.getUuid();
        return uuidMatches(uuid.toString(), uuidString);
    }

    private static boolean isMatchingCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (characteristic == null) {
            return false;
        }
        UUID uuid = characteristic.getUuid();
        return matchesCharacteristicUuidString(uuid.toString());
    }

    private static boolean matchesCharacteristicUuidString(String characteristicIdString) {
        return uuidMatches(characteristicIdString, CHARACTERISTIC_CLIENT_MESSAGE_STRING, CHARACTERISTIC_SERVER_MESSAGE_STRING);
    }

    /**
     * Finds out if the specified characteristic matches with the specified UUID.
     * @param characteristic The specified Characteristic
     * @return Boolean
     */
    public static boolean requiresResponse(BluetoothGattCharacteristic characteristic) {
        return (characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE)
                != BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE;
    }

    /**
     * Finds out if the specified characteristic matches with the specified UUID.
     * @param characteristic The specified Characteristic
     * @return Boolean
     */
    public static boolean requiresConfirmation(BluetoothGattCharacteristic characteristic) {
        return (characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_INDICATE)
                == BluetoothGattCharacteristic.PROPERTY_INDICATE;
    }

    // Descriptor

    /**
     * Finds the clientConfiguration Descriptor.
     * @param descriptorList A list of descriptors.
     * @return The ClientConfiguration
     */
    @Nullable
    public static BluetoothGattDescriptor findClientConfigurationDescriptor(List<BluetoothGattDescriptor> descriptorList) {
        for(BluetoothGattDescriptor descriptor : descriptorList) {
            if (isClientConfigurationDescriptor(descriptor)) {
                return descriptor;
            }
        }

        return null;
    }

    private static boolean isClientConfigurationDescriptor(BluetoothGattDescriptor descriptor) {
        if (descriptor == null) {
            return false;
        }
        UUID uuid = descriptor.getUuid();
        String uuidSubstring = uuid.toString().substring(4, 8);
        return uuidMatches(uuidSubstring, CLIENT_CONFIGURATION_DESCRIPTOR_SHORT_ID);
    }

    // Service

    private static boolean matchesServiceUuidString(String serviceIdString) {
        return uuidMatches(serviceIdString, SERVICE_STRING);
    }

    @Nullable
    private static BluetoothGattService findService(List<BluetoothGattService> serviceList) {
        for (BluetoothGattService service : serviceList) {
            String serviceIdString = service.getUuid()
                    .toString();
            if (matchesServiceUuidString(serviceIdString)) {
                return service;
            }
        }
        return null;
    }

    // String matching
    private static boolean uuidMatches(String uuidString, String... matches) {
        for (String match : matches) {
            if (uuidString.equalsIgnoreCase(match)) {
                return true;
            }
        }

        return false;
    }
}
