package org.libble.android;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.libble.android.util.StringUtils;

/**
 * <h1>Sensor</h1>
 * <p>The Sensor class extends BluetoothServer and adds functionality to make it act as a Sensor. The Sensor class is meant to send data to a BluetoothClient, in a way it requests. </p>
 *
 */
public class Sensor extends BluetoothServer {

    private List<String> data;

    public Sensor(){
        super();
        data = new ArrayList<String>(Arrays.asList( // Dummy data.
                "Date 30:03: A dog walked by.",
                "Date 31.03: No one here.",
                "Date 01.04: No one here.",
                "Date 02.04: Two birds flew by.",
                "Date 03.04: A bird flew by.",
                "Date 03.05: This is a very long chain of data, this is to test the capability of sending larger pieces of data than 20 bytes."));

    }

    @Override
    public byte[] handleMessage(byte[] msgBytes, String sender){
        msgBytes = super.handleMessage(msgBytes, sender);

        /*
            Checks to see what the client wants to be sent.
         */
        switch (msgBytes[0]){
            case 0: // Get the latest data.
                sendMessage(StringUtils.bytesFromString(data.get(data.size()-1)), sender);
                break;
            case 1: // Get the latest data in the specified period.
                if (msgBytes[1] < data.size()){
                    for (int i = data.size() - 1; i >= data.size() - 1 - msgBytes[1]; i--){
                        sendMessage(StringUtils.bytesFromString(data.get(i)), sender);
                    }
                }
                else{
                    sendMessage(StringUtils.bytesFromString("Out of bounds!"), sender);
                }
                break;

            case 2: // Get the data at the specified location.
                if (msgBytes[1] < data.size()){
                    sendMessage(StringUtils.bytesFromString(data.get(data.size() - 1 - msgBytes[1])), sender);
                }
                else{
                    sendMessage(StringUtils.bytesFromString("Out of bounds!"), sender);
                }
                break;
        }

        return msgBytes;
    }

    /**
     * Recieves the list of data held by the Sensor.
     * @return nothing
     */
    public List<String> getData() {
        return data;
    }
    /**
     * Sets a list as the data.
     * @param data
     * @return nothing
     */
    public void setData(List<String> data) {
        this.data = data;
    }
}
