package libble.android.sample;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ListView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.libble.android.BLEConfig;
import org.libble.android.BluetoothClient;
import org.libble.android.interfaces.ClientListener;


public class BeaconActivity extends AppCompatActivity implements ClientListener {
    public static BluetoothClient client;

    String TAG = "BeaconActivity";
    Context instance;
    
    List<ScanItem> scanResult;
    ListView scanResultView;
    int activePosition = -1;

    public static Class nextActivity;
    private Button scan_button;
    private TextView beacon1;
    private TextView beacon2;
    private TextView beacon1_rssi;
    private TextView beacon2_rssi;
    private TextView first_code;
    private TextView second_code;
    private Map<String, String> beacons = new HashMap<>();
    private Map<TextView, String> status = new HashMap<>();
    private Map<TextView, String> rssi = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beacon);

        instance = this;

        scan_button = findViewById(R.id.scan_btn);
        scan_button.setOnClickListener(this::scanForBeacon);
        beacon1 = findViewById(R.id.beacon1);
        status.put(beacon1, "false");
        beacon2 = findViewById(R.id.beacon2);
        status.put(beacon2, "false");
        beacons.put("DD:5B:A1:03:56:96", "beacon1");
        beacons.put("DC:49:AD:EB:79:05", "beacon2");

        beacon1_rssi = findViewById(R.id.beacon1_rssi);
        rssi.put(beacon1_rssi, null);
        beacon2_rssi = findViewById(R.id.beacon2_rssi);
        rssi.put(beacon2_rssi, null);

        first_code = findViewById(R.id.first_code);
        second_code = findViewById(R.id.second_code);


        /*
        if (savedInstanceState != null) {
            nextActivity = (Class) savedInstanceState.get("class");
        }
        */

        BLEConfig.getInstance().initiate(this);
        client = new BluetoothClient();
        client.addCallback(this);
        scanResult = new ArrayList<>();

        scanResultView = findViewById(R.id.scan_overview);
        String[] str = {"Press scan to, well..., scan!"};
        ArrayAdapter adapter = new ArrayAdapter<String>(this,R.layout.scan_list_view,str);
        scanResultView.setAdapter(adapter);

    }

    private void scanForBeacon(View view) {
        if (!checkPermissions()) {
            Log.d(TAG, "Does not have permissions!");
            return;
        }

        String[] str = {"Scanning..."};
        ArrayAdapter adapter = new ArrayAdapter<Object>(this,R.layout.scan_list_view,str);
        scanResultView.setAdapter(adapter);

        client.startDeviceScan("helloUSB");
    }

    private void handleStatusChange(){
        for (Map.Entry<TextView, String> entry : status.entrySet()) {
            if (entry.getValue() == "true") {
                String inRange = entry.getKey().getText().subSequence(0, 10).toString() + "In range";
                entry.getKey().setText(inRange);
                entry.getKey().setTextColor(Color.parseColor("#8bd8c6"));
            } else {
                String outOfRange = entry.getKey().getText().subSequence(0, 10).toString() + "Not in range";
                entry.getKey().setText(outOfRange);
                entry.getKey().setTextColor(Color.parseColor("#f76c99"));
                if (entry.getKey() == beacon1) {
                    beacon1_rssi.setText("");
                } else {
                    beacon2_rssi.setText("");
                }
            }
        }
    }

    private void showCode(int index){
        if (index == 1){
            first_code.setText("Love you");
        } else{
            second_code.setText("3000 <3");
        }
    }


    private void handleRssiChange(){
        for (Map.Entry<TextView, String> entry : status.entrySet()){
            if(entry.getValue() == "true"){
                if(entry.getKey() == beacon1){
                    beacon1_rssi.setText(rssi.get(beacon1_rssi));
                } else {
                    beacon2_rssi.setText(rssi.get(beacon2_rssi));
                }
            }
        }
    }



    @Override
    public void clientScanCallback(List<String> bleDevices) {
        Object[] str;
        scanResult.clear();
        updateStatus(bleDevices);

        for (String o : bleDevices) {
            scanResult.add(new ScanItem(client.getDeviceName(o), o));
        }

        str = scanResult.toArray();
        ArrayAdapter adapter;
        if (str.length == 0) {
            String[] nullStr = {"Could not find any devices... :( Try again!"};
            adapter = new ArrayAdapter<Object>(this, R.layout.scan_list_view, nullStr);
        } else{
            String[] outputStr = {"Scan complete! Click button again to update."};
            adapter=new ArrayAdapter<Object>(this,R.layout.scan_list_view,outputStr);
        }
        scanResultView.setAdapter(adapter);

        updateRssi(client.getRssi());
    }

    private void updateStatus(List<String> bleDevices){
        status.put(beacon1, "false");
        status.put(beacon2, "false");

        for (String o : bleDevices) {
            System.out.println("found: " + beacons.get(o));
            if(beacons.get(o) == "beacon1"){
                status.put(beacon1, "true");
            } else if(beacons.get(o) == "beacon2"){
                status.put(beacon2, "true");
            }
        }
        handleStatusChange();

    }

    private void updateRssi(Map<String, Integer> results){
        for(Map.Entry<String, Integer> entry : results.entrySet()){
            String currentBeacon = findBeacon(entry.getKey());
            System.out.println(currentBeacon.substring(6));
            double distance = getDistance(entry.getValue());
            String outputStr = "";
            if(distance == 10.0) {
                outputStr = ">" + distance + " m";
            } else if(distance == 5.0){
                outputStr = ">" + distance + " m";
            } else if(distance >= 1.0){
                outputStr = "~" + distance + " m";
            } else{
                // show distance in meters
                distance = distance * 100;
                DecimalFormat df = new DecimalFormat("#.##");
                String formatted = df.format(distance);
                outputStr = formatted + " cm";

                // reveal code
                if (distance < 50) {
                    int index = Integer.parseInt(currentBeacon.substring(6));
                    showCode(index);
                }
            }
            switch(currentBeacon){
                case "beacon1":
                    rssi.put(beacon1_rssi, outputStr);
                    break;
                case "beacon2":
                    rssi.put(beacon2_rssi, outputStr);
                    break;
            }
        }
        handleRssiChange();
    }


    /*formula from: https://www.quora.com/How-do-I-calculate-distance-in-meters-km-yards-from-rssi-values-in-dBm-of-BLE-in-android*/
    private double getDistance(int rssi){
        int txPower = -60;

        /*

        distance = 10 ^ ((txPower-RSSI) / (10 ^ N)),

        where N = 2 (constant depends on enviromental factors)

         */
        double distance = Math.pow(10, ((double) txPower - rssi) / (10 * 2));
        if(distance >= 10.0){
            return 10.0;
        } else if(distance >= 5.0){
            return 5.0;
        } else if(distance >= 4.0){
            return 4.0;
        } else if(distance >= 3.0) {
            return 3.0;
        } else if(distance >= 2.0){
              return 2.0;
        } else if (distance >= 1.0){
            return 1.0;
        } else {
            return distance;
        }

    }

    private String findBeacon(String deviceAddress){
        return beacons.get(deviceAddress);
    }


    @Override
    public void clientMessageCallback(byte[] msg) {

    }

    @Override
    public void clientConnectedCallback() {
    }

    @Override
    public void clientDisconnectedCallback() {

    }


    private boolean checkPermissions() {
        if (!BLEConfig.getInstance().hasPermission()) {
            Log.d(TAG, "BLEMISSING!");
            requestBluetoothEnable();
            return false;
        }
        else if (!BLEConfig.getInstance().hasLocationPermissions()) {
            Log.d(TAG, "LOCMISSING!");
            requestLocationPermission();
            return false;
        }

        return true;
    }

    private void requestBluetoothEnable() {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, 1);
        //displayMessage("Requested user enables Bluetooth. Try starting the scan again.");
    }

    private void requestLocationPermission() {
        if (Build.VERSION.SDK_INT > 22) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 2);
        }
    }

    class ScanItem {

        public String name;
        public String value;

        public ScanItem(String name, String value) {
            this.name = name;
            this.value = value;
        }

        @Override
        public String toString() {
            String str = name;
            if (name == null) {
                str = value;
            }

            return str;
        }
    }
}
