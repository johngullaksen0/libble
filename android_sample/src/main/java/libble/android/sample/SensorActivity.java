package libble.android.sample;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import org.libble.android.BLEConfig;
import org.libble.android.BluetoothClient;
import org.libble.android.interfaces.ClientListener;
import org.libble.android.Sensor;
import org.libble.android.interfaces.ServerListener;
import org.libble.android.util.StringUtils;

public class SensorActivity extends AppCompatActivity implements ClientListener, ServerListener {

    String TAG = "SensorActivity";

    BluetoothClient client;
    Sensor server;

    TextView console;

    byte currentAskValue = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor);

        String role = "server";

        //BLEConfig.getInstance().initiate(this);
        role =  getIntent().getExtras().getString("role");
        Log.d(TAG, role + " Doing this!!!");



        console = findViewById(R.id.console_view);

        if (role.equals("SERVER")) {
            server = new Sensor();
            server.addCallback(SensorActivity.this);
            server.start();
            displayMessage("Hosting sensor");
            Log.d(TAG, "Hooosting!");
        }
        else if (role.equals("CLIENT")) {
            client = ScanActivity.client;
            client.addCallback(this);
            displayMessage("Connected");
            Log.d(TAG, "CLIENTING!!");
        }

        Button latstBtn = findViewById(R.id.latest_btn);
        latstBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                byte[] ask = {0};
                client.sendMessage(ask);
            }
        });
        Button lwrBtn = findViewById(R.id.lwr_btn);
        lwrBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentAskValue > 1) {
                    currentAskValue--;
                }
                displayMessage("Ask value is now " + currentAskValue);

            }
        });
        Button hgrBtn = findViewById(R.id.hgr_btn);
        hgrBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentAskValue++;

                displayMessage("Ask value is now " + currentAskValue);
            }
        });
        Button askBtn = findViewById(R.id.ask_btn);
        askBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                byte[] ask = {2, currentAskValue};
                client.sendMessage(ask);
            }
        });
        Button askLtsBtn = findViewById(R.id.ask_latest_btn);
        askLtsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                byte[] ask = {1, currentAskValue};
                client.sendMessage(ask);

            }
        });
        Button clearBtn = findViewById(R.id.clear_btn);
        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                console.setText("");
            }
        });

        //Removes unnecessary buttons if server.
        if (role.equals("SERVER")) {
            ViewGroup layout = (ViewGroup) latstBtn.getParent();
            if(null!=layout) {
                layout.removeView(latstBtn);
                layout.removeView(lwrBtn);
                layout.removeView(hgrBtn);
                layout.removeView(askBtn);
                layout.removeView(askLtsBtn);
            }
        }

    }

    synchronized public void displayMessage(String msg) {
        console.append(msg + "\n");
    }

    @Override
    public void clientScanCallback(List<String> bleDevices) {
        if (bleDevices.isEmpty()) {
            displayMessage("No sensors found!");
            return;
        }

        displayMessage("Connecting to " + bleDevices.get(0));
        client.connectDevice(bleDevices.get(0));

    }

    @Override
    public void clientMessageCallback(byte[] msg) {
        displayMessage(StringUtils.stringFromBytes(msg));
    }

    @Override
    public void clientConnectedCallback() {
        displayMessage("Connected to " + client.getServerAddress());
    }

    @Override
    public void clientDisconnectedCallback() {
        displayMessage("Disconnected from " + client.getServerAddress());
    }


    @Override
    public void serverMessageCallback(byte[] msg, String sender) {
        displayMessage(StringUtils.stringFromBytes(msg));
    }

    @Override
    public void serverDisconnectedCallback(String address) {
        displayMessage("Device removed " + address);
    }

    @Override
    public void serverConnectedCallback(String address) {
        displayMessage("Device added " + address);
    }

    private boolean checkPermissions() {
        if (!BLEConfig.getInstance().hasPermission()) {
            Log.d(TAG, "BLEMISSING!");
            requestBluetoothEnable();
            return false;
        }
        else if (!BLEConfig.getInstance().hasLocationPermissions()) {
            Log.d(TAG, "LOCMISSING!");
            requestLocationPermission();
            return false;
        }

        return true;
    }

    private void requestBluetoothEnable() {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, 1);
        //displayMessage("Requested user enables Bluetooth. Try starting the scan again.");
    }

    private void requestLocationPermission() {
        //Uncomment next line when using api 23 or above
        //requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 2);
        //displayMessage("Requested user enable Location. Try starting the scan again.");
    }
    @Override
    public void onBackPressed() {
        Intent gotoScanIntent = new Intent(this, MainActivity.class);
        if (client != null) {
            client.disconnect();
        }
        if (server != null) {
            server.stop();
        }
        startActivity(gotoScanIntent);
    }
}
