package libble.android.sample;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import org.libble.android.BLEConfig;
import org.libble.android.BluetoothClient;
import org.libble.android.ClientNode;
import org.libble.android.interfaces.NodeListener;
import org.libble.android.util.StringUtils;


public class NodeActivity extends AppCompatActivity implements NodeListener {

    String TAG = "NodeActivity";

    ClientNode client;
    boolean _hosting = false;

    TextView console;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_node);
        console = findViewById(R.id.txtView);
        client = new ClientNode();

        String role = "SERVER";

        //BLEConfig.getInstance().initiate(this);
        role =  getIntent().getExtras().getString("role");
        Log.d(TAG, role + " Doing this!!!");

        if (role.equals("SERVER")) {
            client.startHosting();
            client.setTopLevel(true);
            displayMessage("Hosting Node");
            Log.d(TAG, "Hooosting!");
        }
        else if (role.equals("CLIENT")) {
            displayMessage("Connected");
            Log.d(TAG, "CLIENTING!!");
        }

        client.setClient(ScanActivity.client);
        client.setCallback(this);

        Button msgBtn = findViewById(R.id.msg_btn);
        msgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!checkPermissions()) {
                    return;
                }
                //displayMessage("Scanning for sensor!");
                client.sendMessage("TestMSG, a veryveryvery long message!!!! Hey ho what cho gonna do, yehehdhddddddddddddddddddddddddddj , noooo, what!");
            }
        });

        Button hostBtn = findViewById(R.id.host_btn);
        hostBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!checkPermissions()) {
                    return;
                }
                displayMessage("Hosting");
                client.startHosting();
            }
        });

        Button clearBtn = findViewById(R.id.clear_btn);
        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!checkPermissions()) {
                    return;
                }
                console.setText("");
            }
        });

    }

    synchronized public void displayMessage(String msg) {
        console.append(msg + "\n");
    }

    @Override
    public void nodeScanCallback(List<String> bleDevices, BluetoothClient instance) {

    }

    @Override
    public void nodeMessageCallback(byte[] msg, String sender) {
        String str = StringUtils.stringFromBytes(msg);
        displayMessage(str);
    }

    @Override
    public void nodeConnectedCallback(BluetoothClient instance) {
        displayMessage("Connected to server!");
    }

    @Override
    public void nodeDisconnectedCallback(BluetoothClient instance) {
        displayMessage("Disconnected from server!");
    }

    private boolean checkPermissions() {
        if (!BLEConfig.getInstance().hasPermission()) {
            Log.d(TAG, "BLEMISSING!");
            requestBluetoothEnable();
            return false;
        }
        else if (!BLEConfig.getInstance().hasLocationPermissions()) {
            Log.d(TAG, "LOCMISSING!");
            requestLocationPermission();
            return false;
        }

        return true;
    }

    private void requestBluetoothEnable() {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, 1);
        //displayMessage("Requested user enables Bluetooth. Try starting the scan again.");
    }

    private void requestLocationPermission() {
        //Uncomment next line when using api 23 or above
        //requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 2);
        //displayMessage("Requested user enable Location. Try starting the scan again.");
    }

    @Override
    public void onBackPressed() {
        Intent gotoScanIntent = new Intent(this, MainActivity.class);
        client.stop();
        client.disconnect();
        startActivity(gotoScanIntent);
    }
}
