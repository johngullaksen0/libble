package libble.android.sample;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;


import libble.android.sample.chat.ChatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button chatButton = findViewById(R.id.chat_btn);
        chatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent gotoChatIntent = new Intent(MainActivity.this, ScanActivity.class);
                ScanActivity.nextActivity = ChatActivity.class;
                startActivity(gotoChatIntent);
            }
        });
        Button sensorButton = findViewById(R.id.sensor_btn);
        sensorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent gotoChatIntent = new Intent(MainActivity.this, ScanActivity.class);
                ScanActivity.nextActivity = SensorActivity.class;
                startActivity(gotoChatIntent);
            }
        });
        Button beaconButton = findViewById(R.id.beacon_btn);
        beaconButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent gotoBeaconIntent = new Intent(MainActivity.this, BeaconActivity.class);
                startActivity(gotoBeaconIntent);
            }
        });
    }

}
