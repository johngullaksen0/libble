package libble.android.sample.chat;

import java.io.Serializable;

public class Message implements Serializable {

    private String text;
    private String name;
    private String avatarColor;
    private boolean belongsToCurrentUser;

    public Message(String text, String name, String avatarColor, boolean belongsToCurrentUser) {
        this.text = text;
        this.name = name;
        this.avatarColor = avatarColor;
        this.belongsToCurrentUser = belongsToCurrentUser;
    }

    public String getText() {
        return text;
    }

    public String getName() {
        return name;
    }

    public String getAvatarColor() {
        return avatarColor;
    }

    public boolean belongsToCurrentUser() {
        return belongsToCurrentUser;
    }
}
