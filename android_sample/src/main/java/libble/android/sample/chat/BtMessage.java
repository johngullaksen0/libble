package libble.android.sample.chat;

import android.support.annotation.NonNull;

import com.google.gson.Gson;


public class BtMessage {
    enum Type {
        SET_NAME,       // For asking server to associating the client with this name
        NAME_ACCEPTED,  // BluetoothServer accepts a client name
        NAME_DENIED,    // BluetoothServer denies a client name
        NEW_CLIENT,     // For notifying that a new client has connected
        NAME_ERROR,     // Name is missing, or sender is not associated with given name
        MSG,             // For sending a regular message
        NEW_TOP_LEVEL   // Received when there is a new top level device.
    }

    private Type type;
    private String name;
    private String body;

    private transient static Gson gson = new Gson();

    public BtMessage(@NonNull Type type, String name, String body) {
        this.type = type;
        this.name = name;
        this.body = body;
    }

    public Type getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getBody() {
        return body;
    }

    public String toJson() {
        return gson.toJson(this);
    }

    public static BtMessage fromJson(String json) {
        return gson.fromJson(json, BtMessage.class);
    }

    public byte[] getBytes() {
        return toString().getBytes();
    }

    @Override
    public String toString() {
        return "{" +
                "type=" + type +
                ", name='" + name + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}
