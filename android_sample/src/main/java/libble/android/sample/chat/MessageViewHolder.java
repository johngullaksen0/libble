package libble.android.sample.chat;

import android.view.View;
import android.widget.TextView;

class MessageViewHolder {
    View avatar;
    TextView name;
    TextView messageBody;
}