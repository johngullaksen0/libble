package libble.android.sample.chat;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.libble.android.BLEConfig;
import org.libble.android.BluetoothClient;
import org.libble.android.ClientNode;
import org.libble.android.interfaces.NodeListener;
import org.libble.android.util.StringUtils;
import libble.android.sample.MainActivity;
import libble.android.sample.R;
import libble.android.sample.ScanActivity;

public class ChatActivity extends AppCompatActivity {

    private final String TAG = "ChatActivity";
    private final boolean MULT_LEVEL = false; // Allow using a treelike structure in chat, allowing clients to connect to other clients. Experimental

    private static final String[] PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final int REQUEST_CODE = 0;

    private enum Role {
        CLIENT,
        SERVER
    }
    private Role ROLE;
    private String name;
    private Integer id;
    private int nextClientId = 2;

    private EditText editText;
    private MessageAdapter messageAdapter;
    private ListView messagesView;
    private ClientNode clientNode;
    private HashMap<String, String> clientNames;

    private final NodeListener clientCallback = new NodeListener() {
        @Override
        public void nodeScanCallback(List<String> bleDevices, BluetoothClient instance) {

        }

        @Override
        public void nodeMessageCallback(byte[] msg, String sender) {
            String strmsg = StringUtils.stringFromBytes(msg);
            Log.d(TAG, "REC MESSS/  " + strmsg.toString());
            BtMessage message = BtMessage.fromJson(strmsg);

            if (clientNode.isTopLevel()) {
                Log.d(TAG, "TOP LEVEL/" + message.toString());

                switch (message.getType()) {
                    case SET_NAME:
                        clientNames.remove(message.getName());
                        if (!clientNames.containsKey(message.getName())) {
                            clientNames.put(message.getName(), sender);

                            BtMessage response = new BtMessage(BtMessage.Type.NAME_ACCEPTED, message.getName(), null);
                            clientNode.sendMessage(response.getBytes(), sender);

                            BtMessage broadcastMsg = new BtMessage(BtMessage.Type.NEW_CLIENT, message.getName(), null);
                            clientNode.sendMessage(broadcastMsg.toJson());

                            displayMessage("System", message.getName() + " connected");
                        } else {
                            BtMessage response = new BtMessage(BtMessage.Type.NAME_DENIED, message.getName(), null);
                            clientNode.sendMessage(response.getBytes(), sender);
                        }
                        break;
                    case MSG:
                        boolean hasName = false;
                        for (Map.Entry<String, String> entry : clientNames.entrySet()) {
                            if (entry.getKey().equals(message.getName()) && entry.getValue().equals(sender)) {
                                hasName = true; break;
                            }
                        }
                        if (hasName) {
                            clientNode.sendMessage(message.toJson());
                            displayMessage(message.getName(), message.getBody());
                        } else {
                            BtMessage errorMessage = new BtMessage(BtMessage.Type.NAME_ERROR, message.getName(), null);
                            clientNode.sendMessage(errorMessage.toJson());
                        }
                        break;

                    default: Log.d(TAG, "BluetoothServer received message of wrong type");
                }
            }
            else {
                Log.d(TAG, "LOW LEVEL/" + message.toString());

                switch (message.getType()) {
                    case NAME_ACCEPTED:
                        if (message.getName().equals(ChatActivity.this.name)) {
                            runOnUiThread(() -> Toast.makeText(ChatActivity.this, "Name " + message.getName() + " accepted", Toast.LENGTH_LONG).show());
                        }
                        break;
                    case NAME_DENIED:
                        if (message.getName().equals(ChatActivity.this.name)) {
                            runOnUiThread(() -> Toast.makeText(ChatActivity.this, "Name " + message.getName() + " denied", Toast.LENGTH_LONG).show());
                            name = null;
                        }
                        break;
                    case NAME_ERROR:
                        if (message.getName().equals(ChatActivity.this.name)) {
                            runOnUiThread(() -> Toast.makeText(ChatActivity.this, "Name " + message.getName() + " error", Toast.LENGTH_LONG).show());
                            name = null;
                        }
                        break;
                    case NEW_CLIENT:
                        displayMessage("System", message.getName() + " connected");
                        break;
                    case NEW_TOP_LEVEL:
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        clientNode.sendMessage(new BtMessage(BtMessage.Type.SET_NAME, name, null).toJson());
                        runOnUiThread(() -> Toast.makeText(ChatActivity.this, "Top level lost, resending name.", Toast.LENGTH_LONG).show());
                        break;
                    case MSG:
                        if (!message.getName().equals(name)) {
                            displayMessage(message.getName(), message.getBody());
                        }
                        break;
                    default: Log.d(TAG, "BluetoothClient received message of wrong type");
                }

            }

        }

        @Override
        public void nodeConnectedCallback(BluetoothClient instance) {}

        @Override
        public void nodeDisconnectedCallback(BluetoothClient instance) {
            runOnUiThread(() -> Toast.makeText(ChatActivity.this, "Lost connection", Toast.LENGTH_LONG).show());

            if (clientNode.isHosting()) {
                clientNode.setTopLevel(true);
                clientNames.put(ChatActivity.this.name, "SERVER");

                BtMessage broadcastMsg = new BtMessage(BtMessage.Type.NEW_TOP_LEVEL, null, null);
                clientNode.sendMessage(broadcastMsg.toJson());
            }
            else {
                onBackPressed();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        if (savedInstanceState != null) {
            name = savedInstanceState.getString("name");
            ROLE = (Role) savedInstanceState.get("role");
            messageAdapter = (MessageAdapter) savedInstanceState.get("messages");
            clientNames = (HashMap<String, String>) savedInstanceState.get("client_names");
        }

        if (messageAdapter == null) {
            messageAdapter = new MessageAdapter(this);
        }

        if (clientNames == null) {
            clientNames = new HashMap<>();
        }

        messagesView = findViewById(R.id.messages_view);
        messagesView.setAdapter(messageAdapter);

        if (ROLE == null) {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                ROLE = Role.valueOf(extras.getString("role"));
            }
        }

        clientNode = new ClientNode();
        switch (ROLE) {
            case CLIENT:
                Log.d(TAG, "CLIENT! init");
                break;
            case SERVER:
                clientNode.startHosting();
                clientNode.setTopLevel(true);
                Log.d(TAG, "SERVER! init");
                break;
        }
        clientNode.setClient(ScanActivity.client);
        clientNode.setCallback(clientCallback);

        editText = findViewById(R.id.edit_text);

        ImageButton sendBtn = findViewById(R.id.send_btn);
        sendBtn.setOnClickListener(this::sendMessage);

        Button hostBtn = findViewById(R.id.host_btn);
        hostBtn.setOnClickListener(this::startHost);
        if (!MULT_LEVEL) {
            ViewGroup layout = (ViewGroup) hostBtn.getParent();
            if(null!=layout)
                layout.removeView(hostBtn);
        }


        hasPermission();


        if (name == null) {
            askForName();
        }
    }

    private void askForName() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Enter name:");

        final EditText input = new EditText(this);

        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton("OK", (dialog, which) -> {
            name = input.getText().toString();
            if (ROLE == Role.SERVER) {
                clientNames.put(name, "SERVER");
            }
            if (name.equals("")) {
                Toast.makeText(this, "Empty name", Toast.LENGTH_SHORT).show();
            } else if (ROLE == Role.CLIENT && id == null) {
                clientNode.sendMessage(new BtMessage(BtMessage.Type.SET_NAME, name, null).toJson());
            }
        });

        builder.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (clientNode != null) {
            //btServer.stop();
            clientNode.stop();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putString("name", name);
        savedInstanceState.putSerializable("role", ROLE);
        savedInstanceState.putSerializable("messages", messageAdapter);
        savedInstanceState.putSerializable("client_names", clientNames);
    }

    private void sendMessage(View view) {
        if (hasPermission()) {
            if (name == null || name.equals("")) {
                askForName();
            } else {
                String body = editText.getText().toString();
                if (body.length() > 0) {
                    BtMessage message = new BtMessage(BtMessage.Type.MSG, name, body);

                    clientNode.sendMessage(message.toJson());

                    editText.getText().clear();
                    displayMessage(message.getName(), message.getBody());
                }
            }
        }
    }

    private void startHost(View view) {
        if (!clientNode.isHosting()) {
            clientNode.startHosting();
            runOnUiThread(() -> Toast.makeText(ChatActivity.this, "Started Hosting", Toast.LENGTH_LONG).show());
        }
        else {
            runOnUiThread(() -> Toast.makeText(ChatActivity.this, "Already Hosting!", Toast.LENGTH_LONG).show());
        }
    }

    private void displayMessage(String name, String content) {
        Message message;
        if (name.equals(this.name)) {
            message = new Message(content, "Me", "#00FF00", true);
        } else {
            message = new Message(content, name, getRandomColor(name), false);
        }

        runOnUiThread(() -> {
            messageAdapter.add(message);
            messagesView.setSelection(messagesView.getCount() - 1);
        });
    }

    private static String getRandomColor(String seed) {
        int seedInt = 0;
        for (byte x : seed.getBytes()) {
            seedInt += x;
        }
        Random random = new Random();
        random.setSeed(seedInt);
        int hex = random.nextInt(0xffffff);
        return String.format("#%06x", hex);
    }

    private void requestLocationPermission() {
        if (Build.VERSION.SDK_INT > 22) {
            requestPermissions(PERMISSIONS, REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE) {
            if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "The app need this permission to function", Toast.LENGTH_LONG).show();
            }
        }
    }


    private void requestBluetoothEnable() {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, 1);
    }

    private boolean hasPermission() {
        String TAG = "ChatActivity";
        if (!BLEConfig.getInstance().hasPermission()) {
            Log.d(TAG, "Bluetooth is currently disabled");
            requestBluetoothEnable();
            return false;
        }
        else if (!BLEConfig.getInstance().hasLocationPermissions()) {
            Log.d(TAG, "Location permission has not been granted");
            requestLocationPermission();
            return false;
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        Intent gotoScanIntent = new Intent(this, MainActivity.class);
        clientNode.disconnect();
        clientNode.destroy();
        startActivity(gotoScanIntent);
    }
}
