package libble.android.sample;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.widget.AdapterView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import org.libble.android.*;
import org.libble.android.interfaces.ClientListener;


public class ScanActivity extends AppCompatActivity implements ClientListener {
    public static BluetoothClient client;

    String TAG = "ScanActivity";
    Context instance;
    
    List<ScanItem> scanResult;
    ListView scanResultView;
    int activePosition = -1;

    public static Class nextActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        instance = this;

        BLEConfig.getInstance().initiate(this);
        client = new BluetoothClient();
        client.addCallback(this);
        scanResult = new ArrayList<>();

        scanResultView = findViewById(R.id.scan_overview);
        scanResultView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        String[] str = {"Press 'Scan' to start scanning."};
        ArrayAdapter adapter = new ArrayAdapter<String>(this,R.layout.scan_list_view,str);
        scanResultView.setAdapter(adapter);
        scanResultView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                scanResultView.setItemChecked(position, true);
                activePosition = position;
                Log.d(TAG, "At position " + activePosition);
                if (!scanResult.isEmpty()) {
                    new AlertDialog.Builder(instance)
                            .setTitle("Connect")
                            .setMessage("Do you want to connect to this device?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    connectToChat(arg1);
                                }
                            })
                            .setNegativeButton(android.R.string.no, null)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            }
        });

        Button hostButton = findViewById(R.id.goto_chat_btn);
        hostButton.setOnClickListener(this::hostChat);
        Button scanButton = findViewById(R.id.scan_btn);
        scanButton.setOnClickListener(this::scanForChat);
    }

    private void hostChat(View view) {
        if (checkPermissions()) {
            Intent gotoChatIntent = new Intent(this, nextActivity);
            gotoChatIntent.putExtra("role", "SERVER");
            startActivity(gotoChatIntent);
        }
    }
    private void scanForChat(View view) {
        if (!checkPermissions()) {
            Log.d(TAG, "Does not have permissions!");
            return;
        }

        String[] str = {"Scanning..."};
        ArrayAdapter adapter = new ArrayAdapter<Object>(this,R.layout.scan_list_view,str);
        scanResultView.setAdapter(adapter);

        client.startScan();

    }


    private void connectToChat(View view) {
        if(activePosition < 0 || scanResult.isEmpty()) {
            Log.d(TAG, "No !");
            return;
        }
        String[] str = {"Press 'Scan' to start scanning."};
        ArrayAdapter adapter = new ArrayAdapter<Object>(this,R.layout.scan_list_view,str);
        scanResultView.setAdapter(adapter);
        client.connectDevice(scanResult.get(activePosition).value);
        scanResult.clear();
    }

    @Override
    public void clientScanCallback(List<String> bleDevices) {
        Object[] str;
        scanResult.clear();

        for (String o : bleDevices) {
            scanResult.add(new ScanItem(client.getDeviceName(o), o));
        }

        str = scanResult.toArray();
        ArrayAdapter adapter = new ArrayAdapter<Object>(this,R.layout.scan_list_view,str);
        scanResultView.setAdapter(adapter);
    }

    @Override
    public void clientMessageCallback(byte[] msg) {

    }

    @Override
    public void clientConnectedCallback() {
        client.removeCallback(this);
        Intent gotoChatIntent = new Intent(this, nextActivity);
        gotoChatIntent.putExtra("role", "CLIENT");

        startActivity(gotoChatIntent);
    }

    @Override
    public void clientDisconnectedCallback() {

    }

    private boolean checkPermissions() {
        if (!BLEConfig.getInstance().hasPermission()) {
            Log.d(TAG, "BLEMISSING!");
            requestBluetoothEnable();
            return false;
        }
        else if (!BLEConfig.getInstance().hasLocationPermissions()) {
            Log.d(TAG, "LOCMISSING!");
            requestLocationPermission();
            return false;
        }

        return true;
    }

    private void requestBluetoothEnable() {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, 1);
        //displayMessage("Requested user enables Bluetooth. Try starting the scan again.");
    }

    private void requestLocationPermission() {
        if (Build.VERSION.SDK_INT > 22) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 2);
        }
    }

    class ScanItem {
        public String name;
        public String value;

        public ScanItem(String name, String value) {
            this.name = name;
            this.value = value;
        }

        @Override
        public String toString() {
            String str = name;
            if (name == null) {
                str = value;
            }

            return str;
        }
    }
}
