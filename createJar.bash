#!/usr/bin/env bash
SCRIPT=`realpath $0`
SCRIPTPATH=`dirname $SCRIPT`

cd $SCRIPTPATH

./gradlew :linux:jar
./gradlew :android:createFullJarRelease

mkdir tmp
(cd tmp; unzip -uo ../android/build/intermediates/full_jar/release/createFullJarRelease/full.jar)
(cd tmp; unzip -uo ../linux/build/libs/linux-v0.2.1.jar)
rm build/jar/libble.jar
jar -cvf build/jar/libble.jar -C tmp .
rm -rf tmp
